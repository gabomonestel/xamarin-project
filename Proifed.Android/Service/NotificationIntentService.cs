﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Support.V4.App;
using Android.Widget;
using Plugin.LocalNotifications;
using Proifed.Config;
using Proifed.Data;
using Proifed.Models;
using Proifed.Util;
using Proifed.ViewModels;
using Xamarin.Forms;

namespace Proifed.Droid.Service
{
    [Service(Name = "com.uned.proifed.Proifed.Service.NotificationIntentService", Permission = "android.permission.BIND_JOB_SERVICE")]
    public class NotificationIntentService : JobIntentService
    {

        public static void EnqueueWork(Context context, Intent work)
        {
            Java.Lang.Class notificationIntentService = Java.Lang.Class.FromType(typeof(NotificationIntentService));
            EnqueueWork(context, notificationIntentService, 1001, work);
        }

        protected async override void OnHandleWork(Intent intent)
        {
            String horaAlarma = intent.GetStringExtra(Constants.ALARMA_HORA);
            var ingresosViewModel = new IngresosRealesViewModel(DependencyService.Get<ISQLiteConnection>(), DateUtil.FechaFinMes());
            if (DateTime.Now.Day <= 9 && !await ingresosViewModel.IngresosFueronModificados())
            {
                CrossLocalNotifications.Current.Show("No hay datos reales "+ horaAlarma, "Por favor ingrese datos reales a la aplicación", 1001, DateTime.Now.AddSeconds(1));
            }
            /*var ingresosViewModel = new IngresosRealesViewModel(DependencyService.Get<ISQLiteConnection>(), DateUtil.FechaActual());
            var gastosViewModel = new GastosRealesViewModel(DependencyService.Get<ISQLiteConnection>(), DateUtil.FechaActual());

            await ingresosViewModel.CrearIngresosRealesMes();

            await gastosViewModel.CrearGastosRealesMes();*/

        }


    }
}
