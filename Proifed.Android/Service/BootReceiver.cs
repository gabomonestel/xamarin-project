﻿using System;
using Android.App;
using Android.Content;
using Android.Widget;

namespace Proifed.Droid.Service
{
    //[BroadcastReceiver]
    //[IntentFilter(new[] { Intent.ActionBootCompleted })]
    [BroadcastReceiver(Enabled = true, Exported = true, DirectBootAware = true)]
    [IntentFilter(new string[] { Intent.ActionBootCompleted, Intent.ActionLockedBootCompleted, "android.intent.action.QUICKBOOT_POWERON", "com.htc.intent.action.QUICKBOOT_POWERON" })]
    public class BootReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            //Toast.MakeText(context, "Service Running", ToastLength.Long).Show();
            //UpdateIntentService.EnqueueWork(context, intent);
            //NotificationAlarm.SetAlarm(context);
        }
    }
}
