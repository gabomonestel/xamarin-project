﻿using System;
using Android.Content;

namespace Proifed.Droid.Service
{
    [BroadcastReceiver]
    public class NotificationReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            NotificationIntentService.EnqueueWork(context, intent);
        }
    }
}
