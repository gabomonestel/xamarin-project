﻿using System;
using Android.App;
using Android.Content;
using Proifed.Config;
using Xamarin.Essentials;

namespace Proifed.Droid.Service
{
    public class NotificationAlarm
    {
        public static void SetAlarm(Context context)
        {
            Java.Util.Calendar calendarAm = Java.Util.Calendar.Instance;
            calendarAm.Set(Java.Util.CalendarField.HourOfDay, 10);
            calendarAm.Set(Java.Util.CalendarField.Minute, 22);
            calendarAm.Set(Java.Util.CalendarField.Second, 00);
            calendarAm.Add(Java.Util.CalendarField.DayOfMonth, 1);
            Java.Util.Calendar calendarPm = Java.Util.Calendar.Instance;
            calendarPm.Set(Java.Util.CalendarField.HourOfDay, 14);
            calendarPm.Set(Java.Util.CalendarField.Minute, 0);
            calendarPm.Set(Java.Util.CalendarField.Second, 00);
            calendarPm.Add(Java.Util.CalendarField.DayOfMonth, 1);


            Intent alarmIntentAm = new Intent(context, typeof(NotificationReceiver));
            alarmIntentAm.PutExtra(Constants.ALARMA_HORA, "AM");

            Intent alarmIntentPm = new Intent(context, typeof(NotificationReceiver));
            alarmIntentPm.PutExtra(Constants.ALARMA_HORA, "PM");

            PendingIntent pendingIntent6am = PendingIntent.GetBroadcast(context, 1001, alarmIntentAm, PendingIntentFlags.UpdateCurrent);
            PendingIntent pendingIntent2pm = PendingIntent.GetBroadcast(context, 1002, alarmIntentPm, PendingIntentFlags.UpdateCurrent);
            AlarmManager alarmManager = (AlarmManager)context.GetSystemService(Context.AlarmService);

            long intervalo = 24 * 60 * 60 * 1000;

            alarmManager.SetRepeating(AlarmType.RtcWakeup, calendarAm.TimeInMillis, intervalo, pendingIntent6am);
            alarmManager.SetRepeating(AlarmType.RtcWakeup, calendarPm.TimeInMillis, intervalo, pendingIntent2pm);

            PendingIntent pendingIntentAm = PendingIntent.GetBroadcast(context, 1001, alarmIntentAm, 0);
            PendingIntent pendingIntentPm = PendingIntent.GetBroadcast(context, 1002, alarmIntentAm, 0);

            Preferences.Set(Constants.NOTIFICACIONES_CONFIGURADAS, true);
        }
}
}
