﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Proifed.Data;
using Proifed.Models;
using Xamarin.Forms;
using System.Collections.Generic;
using Proifed.Droid.Service;
using Xamarin.Essentials;
using Proifed.Config;

namespace Proifed.Droid
{
    [Activity(Label = "Proifed", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);

            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            Xamarin.Forms.DependencyService.Register<SQLiteConnectionDroid>();

            LoadApplication(new App());

            if (!Preferences.Get(Constants.NOTIFICACIONES_CONFIGURADAS, false))
            {
                NotificationAlarm.SetAlarm(this);
            }


        }

        public override void OnBackPressed()
        {
            base.OnBackPressed();
        }

    }
}