﻿using System;
using System.IO;
using SQLite;

namespace Proifed.Droid
{
    public class SQLiteConnectionDroid : ISQLiteConnection
    {
        private SQLiteAsyncConnection _connection;

        public SQLiteConnectionDroid()
        {
        }

        public SQLiteAsyncConnection GetConnection()
        {
            if (_connection != null)
            {
                return _connection;
            }

            SQLitePCL.Batteries.Init();
            return _connection = new SQLiteAsyncConnection(GetDataBasePath());
        }

        public string GetDataBasePath()
        {
            string filename = "ProifedDb.db3";
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

            return Path.Combine(path, filename);
        }
    }
}
