﻿using System;
using System.IO;
using SQLite;

namespace Proifed.iOS
{
    public class SQLiteConnectionTouch : ISQLiteConnection
    {
        private SQLiteAsyncConnection _connection;

        public SQLiteConnectionTouch()
        {
        }

        public SQLiteAsyncConnection GetConnection()
        {
            if (_connection != null)
            {
                return _connection;
            }

            SQLitePCL.Batteries.Init();
            return _connection = new SQLiteAsyncConnection(GetDataBasePath());
        }

        public string GetDataBasePath()
        {
            string filename = "ProifedDb.db3";
            string docFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libFolder = Path.Combine(docFolder, "..", "Library", "Databases");

            if (!Directory.Exists(libFolder))
            {
                System.IO.Directory.CreateDirectory(libFolder);
            }

            return Path.Combine(libFolder, filename);
        }
    }
}
