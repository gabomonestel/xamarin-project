﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using Plugin.LocalNotifications;
using Proifed.Data;
using Proifed.Models;
using Proifed.Util;
using Proifed.ViewModels;
using UIKit;
using UserNotifications;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Proifed.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();
            Xamarin.Forms.DependencyService.Register<SQLiteConnectionTouch>();

            UIApplication.SharedApplication.SetMinimumBackgroundFetchInterval(900);//86400);

            LoadApplication(new App());

            // Ask the user for permission to get notifications on iOS 10.0+
            UNUserNotificationCenter.Current.RequestAuthorization(
                UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound,
                (approved, error) => {
                    if (approved)
                    {
                        UNUserNotificationCenter.Current.Delegate = new UserNotificationCenterDelegate();
                    }
                });

            return base.FinishedLaunching(app, options);
        }


        /*private override void SetMinimumBackgroundFetchInterval()
        {
            UIApplication.SharedApplication.SetMinimumBackgroundFetchInterval(86400);
        }*/

        // Called whenever your app performs a background fetch
        public override void  PerformFetch(UIApplication application, Action<UIBackgroundFetchResult> completionHandler)
        {
            // Do Background Fetch
            var downloadSuccessful = true;

            /*Preferences.Set("Background", "Se ejecutó en segundo plano");

            var ingresosViewModel = new IngresosRealesViewModel(DependencyService.Get<ISQLiteConnection>(), DateUtil.FechaActual());
            var gastosViewModel = new GastosRealesViewModel(DependencyService.Get<ISQLiteConnection>(), DateUtil.FechaActual());

            await ingresosViewModel.CrearIngresosRealesMes();

            await gastosViewModel.CrearGastosRealesMes();
            */

            completionHandler(downloadSuccessful ? UIBackgroundFetchResult.NewData : UIBackgroundFetchResult.NoData);
        }

    }

    public class UserNotificationCenterDelegate : UNUserNotificationCenterDelegate
    {
        public UserNotificationCenterDelegate()
        {
            ProgramarNotificaciones();
        }

        public override void DidReceiveNotificationResponse(UNUserNotificationCenter center, UNNotificationResponse response, Action completionHandler)
        {
            ProgramarNotificaciones();
            completionHandler();
        }

        public override async void WillPresentNotification(UNUserNotificationCenter center, UNNotification notification, Action<UNNotificationPresentationOptions> completionHandler)
        {
            ProgramarNotificaciones();
            var ingresosViewModel = new IngresosRealesViewModel(DependencyService.Get<ISQLiteConnection>(), DateUtil.FechaFinMes());
            if (DateTime.Now.Day <= 9 && !await ingresosViewModel.IngresosFueronModificados())
            {
                //CrossLocalNotifications.Current.Show("No hay datos reales", "Por favor ingrese datos reales a la aplicación", 1001, DateTime.Now.AddSeconds(1));
                completionHandler(UNNotificationPresentationOptions.Sound | UNNotificationPresentationOptions.Alert);
            }
            else
            {
                completionHandler(UNNotificationPresentationOptions.None);
            }
        }

        public void ProgramarNotificaciones()
        {
            DateTime calendarAm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 9, 0);
            DateTime calendarPm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0);

            if (calendarAm.Ticks < DateTime.Now.Ticks)
            {
                calendarAm = calendarAm.AddDays(1);
            }
            if (calendarPm.Ticks < DateTime.Now.Ticks)
            {
                calendarPm = calendarPm.AddDays(1);
            }
            CrossLocalNotifications.Current.Show("No hay datos reales Am", "Por favor ingrese datos reales a la aplicación", 1001, calendarAm);
            CrossLocalNotifications.Current.Show("No hay datos reales Pm", "Por favor ingrese datos reales a la aplicación", 1002, calendarPm);
        }
    }
}
