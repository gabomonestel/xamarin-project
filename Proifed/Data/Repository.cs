﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;

namespace Proifed.Data
{
    public class Repository<T> : IRepository<T> where T : class, new()
    {
        //ISQLiteConnection _connectionService;
        SQLiteAsyncConnection _connection;

        public Repository(ISQLiteConnection connectionService)
        {
            //_connectionService = connectionService;
            //_connection = _connectionService.GetConnection();
            _connection = connectionService.GetConnection();
            _connection.CreateTableAsync<T>();
        }

        public Task<List<T>> GetAllAsync()
        {
            return _connection.Table<T>().ToListAsync();
        }

        public Task<List<T>> Query(string q)
        {
            return _connection.QueryAsync<T>(q);
        }

        public Task<T> Get(int id)
        {
            return _connection.GetAsync<T>(id);
        }

        public Task<int> Insert(T entity)
        {
            return _connection.InsertAsync(entity);
        }

        public Task<int> InsertOrReplace(T entity)
        {
            return _connection.InsertOrReplaceAsync(entity);
        }

        public Task<int> Update(T entity)
        {
            return _connection.UpdateAsync(entity);
        }

        public Task<int> Delete(T entity)
        {
            return _connection.DeleteAsync(entity);
        }

        Task<int> IRepository<T>.DeleteAll()
        {
            return _connection.DeleteAllAsync<T>();
        }

        public Task<int> Execute(string query, params object[] args)
        {
            return _connection.ExecuteAsync(query, args);
        }

        public AsyncTableQuery<T> QuerableTable()
        {
            return _connection.Table<T>();
        }

        public Task<int> Count()
        {
            return _connection.Table<T>().CountAsync();
        }

    }
}
