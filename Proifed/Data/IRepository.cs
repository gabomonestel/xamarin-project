﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;

namespace Proifed.Data
{
    public interface IRepository<T> where T : class, new()
    {
        Task<List<T>> GetAllAsync();
        Task<List<T>> Query(string query);
        Task<T> Get(int id);
        Task<int> Insert(T entity);
        Task<int> InsertOrReplace(T entity);
        Task<int> Update(T entity);
        Task<int> Delete(T entity);
        Task<int> DeleteAll();
        Task<int> Execute(string query, params object[] args);
        AsyncTableQuery<T> QuerableTable();
        Task<int> Count();
    }
}
