﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Proifed.Views;
using Proifed.ViewModels;
using Proifed.Util;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Proifed
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            //MainPage = new SplashPage();
            MainPage = new NavigationPage( new SplashPage()){
                BarBackgroundColor = Color.FromHex("#fd8000"), 
                BarTextColor = Color.White
            };
            /*ComprobarGenerarIngresosReales();
            ComprobarGenerarGastosReales();
            ComprobarGeneraAhorros();*/
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }


        /*private void ComprobarGenerarIngresosReales()
        {
            IngresosRealesViewModel viewModel = new IngresosRealesViewModel(DependencyService.Get<ISQLiteConnection>(), DateUtil.FechaFinMes());
            viewModel.CrearIngresosRealesMes();
        }

        private void ComprobarGenerarGastosReales()
        {
            GastosRealesViewModel viewModel = new GastosRealesViewModel(DependencyService.Get<ISQLiteConnection>(), DateUtil.FechaFinMes());
            viewModel.CrearGastosRealesMes();
        }

        private void ComprobarGeneraAhorros()
        {
            AhorrosViewModel viewModel = new AhorrosViewModel(DependencyService.Get<ISQLiteConnection>());
            viewModel.CrearAhorrosMes();
        }*/
    }
}
