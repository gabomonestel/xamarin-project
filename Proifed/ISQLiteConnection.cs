﻿using System;
using SQLite;

namespace Proifed
{
    public interface ISQLiteConnection
    {
        string GetDataBasePath();
        SQLiteAsyncConnection GetConnection();
    }
}
