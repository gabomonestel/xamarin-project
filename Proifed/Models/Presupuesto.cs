﻿using System;
using Proifed.Config;
using SQLite;

namespace Proifed.Models
{
    public class Presupuesto
    {

        public DateTime FechaInicio { get; set; }

        public bool Configurando { set; get; }

        public int PasoActual { set; get; }

        //public Anotacion Anotacion { set; get; }

        public override string ToString()
        {
            return "Presupuesto desde " + FechaInicio.ToString("MMMM yyyy");
        }
    }
}
