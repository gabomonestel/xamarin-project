﻿using System;
namespace Proifed.Models
{
    public class AppMenuItem
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Imagen { get; set; }

        public AppMenuItem(int id, string titulo, string imagen)
        {
            this.Id = id;
            this.Titulo = titulo;
            this.Imagen = imagen;
        }
    }
}
