﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Proifed.Models
{
    public class GrupoItem<K, T> : ObservableCollection<T>
    {
        public K Key { get; private set; }

        public GrupoItem(K key, IEnumerable<T> items)
        {
            Key = key;
            foreach (var item in items)
                this.Items.Add(item);
        }
    }
}
