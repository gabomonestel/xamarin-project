﻿using System;
using SQLite;
namespace Proifed.Models
{
    public class Credito
    {
        [PrimaryKey, AutoIncrement, Column("id")]
        public int Id { set; get; }

        public int Tipo { set; get; }

        public string Descripcion { get; set; }

        public double Monto { set; get; }

        public double CuotaMensual { set; get; }

        public int Plazo { set; get; }

        public DateTime FechaInicio { set; get; }

        public DateTime FechaFinalizacion { set; get; }

        public override string ToString()
        {
            return Descripcion;
        }
    }
}
