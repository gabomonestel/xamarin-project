﻿using System;
using SQLite;

namespace Proifed.Models
{
    public class Anotacion
    {
        [PrimaryKey, AutoIncrement, Column("id")]
        public int Id { get; set; }
        public DateTime Fecha { set; get; }
        public String Descripcion { set; get; }
    }
}
