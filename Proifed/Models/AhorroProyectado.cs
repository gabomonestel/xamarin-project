﻿using System;
using SQLite;

namespace Proifed.Models
{
    public class AhorroProyectado
    {
        [PrimaryKey, AutoIncrement, Column("id")]
        public int Id { set; get; }

        public string Nombre { set; get; }

        public string Descripcion { set; get; }

        public double AporteInicial { set; get; }

        public double Monto { set; get; }

        public double InteresAnnual { set; get; }

        public double InteresMensual { set; get; }

        public DateTime FechaCreacion { get; set; }

        public DateTime FechaFinalizacion { get; set; }

        public DateTime FechaUltimaGeneracion { get; set; }

        public override string ToString()
        {
            return Id + " - " + Nombre;
        }
    }
}
