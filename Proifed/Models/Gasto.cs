﻿using System;
using SQLite;

namespace Proifed.Models
{
    public class Gasto
    {
        [PrimaryKey, AutoIncrement, Column("id")]
        public int Id { set; get; }

        public int Rubro { set; get; }

        public int Tipo { set; get; }

        public string Nombre { set; get; }

        public double Monto { set; get; }

        public DateTime FechaCreacion { get; set; }

        public bool EsReal { set; get; }

        public int IdProyectado { set; get; }

        public bool ModificadoUsuario { set; get; }

        public override string ToString()
        {
            return Id + " - " + Nombre;
        }

    }
}
