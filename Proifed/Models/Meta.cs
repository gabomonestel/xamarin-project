﻿using System;
using SQLite;

namespace Proifed.Models
{
    public class Meta
    {
        [PrimaryKey, AutoIncrement, Column("id")]
        public int Id { set; get; }

        [MaxLength(50)]
        public string Nombre { set; get; }

        [MaxLength(50)]
        public string Descripcion { set; get; }

        public DateTime Finaliza { set; get; }

        [MaxLength(50)]
        public string PlanAccion { set; get; }

        public override string ToString()
        {
            return Id + " - " + Nombre;
        }

    }

}
