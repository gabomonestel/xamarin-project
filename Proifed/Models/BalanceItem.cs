﻿using System;
namespace Proifed.Models
{
    public class BalanceItem
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public double Monto { get; set; }
        public string Color { get; set; }

        public BalanceItem(int id, string titulo, double monto, string color)
        {
            this.Id = id;
            this.Titulo = titulo;
            this.Monto = monto;
            this.Color = color;
        }
    }
}
