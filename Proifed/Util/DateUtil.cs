﻿using System;
namespace Proifed.Util
{
    public class DateUtil
    {
        public DateUtil()
        {

        }

        public static DateTime FechaFinMes()
        {
            return new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1);
        }


        public static DateTime FechaInicioMes()
        {
            return new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        }
    }
}
