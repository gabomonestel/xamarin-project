﻿using System;
using Proifed.Models;

namespace Proifed.Util
{
    public class CopyUtil
    {
        public CopyUtil()
        {
        }

        public static Ingreso CopiarIngresoProyectado(Ingreso proyectado, bool esReal)
        {
            Ingreso lIngreso = new Ingreso();
            lIngreso.Nombre = proyectado.Nombre;
            lIngreso.Tipo = proyectado.Tipo;
            lIngreso.Monto = proyectado.Monto;
            lIngreso.IdProyectado = proyectado.IdProyectado;
            lIngreso.EsReal = esReal;
            lIngreso.ModificadoUsuario = false;
            lIngreso.FechaCreacion = DateUtil.FechaFinMes();
            return lIngreso;
        }


        public static Ingreso CopiarIngresoProyectado(IngresoProyectado proyectado, Ingreso ingreso, bool esReal)
        {
            ingreso.Nombre = proyectado.Nombre;
            ingreso.Tipo = proyectado.Tipo;
            ingreso.Monto = proyectado.Monto;
            ingreso.IdProyectado = proyectado.Id;
            ingreso.EsReal = esReal;
            ingreso.ModificadoUsuario = false;
            ingreso.FechaCreacion = DateUtil.FechaFinMes();

            return ingreso;
        }

        public static Gasto CopiarGastoProyectado(Gasto proyectado, bool esReal)
        {
            Gasto lGasto = new Gasto();
            lGasto.Nombre = proyectado.Nombre;
            lGasto.Rubro = proyectado.Rubro;
            lGasto.Tipo = proyectado.Tipo;
            lGasto.Monto = proyectado.Monto;
            lGasto.IdProyectado = proyectado.IdProyectado;
            lGasto.EsReal = esReal;
            lGasto.ModificadoUsuario = false;
            lGasto.FechaCreacion = DateUtil.FechaFinMes();
            return lGasto;
        }

        public static Gasto CopiarGastoProyectado(GastoProyectado proyectado, Gasto gasto, bool esReal)
        {
            gasto.Nombre = proyectado.Nombre;
            gasto.Rubro = proyectado.Rubro;
            gasto.Tipo = proyectado.Tipo;
            gasto.Monto = proyectado.Monto;
            gasto.IdProyectado = proyectado.Id;
            gasto.EsReal = esReal;
            gasto.ModificadoUsuario = false;
            gasto.FechaCreacion = DateUtil.FechaFinMes();

            return gasto;
        }

        public static Ahorro CopiarAhorroProyectado(AhorroProyectado proyectado, Ahorro ahorro)
        {
            ahorro.Nombre = proyectado.Nombre;
            ahorro.Descripcion = proyectado.Descripcion;
            ahorro.Monto = proyectado.Monto;
            ahorro.IdProyectado = proyectado.Id;
            ahorro.ModificadoUsuario = false;
            ahorro.FechaCreacion = DateUtil.FechaFinMes();

            return ahorro;
        }



    }
}
