﻿using System;
namespace Proifed.Config
{
    public static class Constants
    {
        public const string PRESUPUESTO = "presupuesto";
        public const string INGRESOS_GENERADOS = "ingresos_generados";
        public const string GASTOS_GENERADOS = "gastos_generados";
        public const string NOTIFICACIONES_CONFIGURADAS = "notificaciones_configuradas";
        public const string ALARMA_HORA = "alarma_hora";
        public const string MOSTRAR_AYUDA_CREACION_PRESUPUESTO = "mostrar_ayuda_creacion_presupuesto";
        public const string MOSTRAR_AYUDA_INGRESOS_REALES = "mostrar_ayuda_ingresos_reales";
        public const string MOSTRAR_AYUDA_INGRESOS_PROYECTADOS = "mostrar_ayuda_ingresos_proyectados";
        public const string MOSTRAR_AYUDA_GASTOS_REALES = "mostrar_ayuda_gastos_reales";
        public const string MOSTRAR_AYUDA_GASTOS_PROYECTADOS = "mostrar_ayuda_gastos_proyectados";
        public const string MOSTRAR_AYUDA_AHORROS = "mostrar_ayuda_ahorros";
        public const string MOSTRAR_AYUDA_CREDITOS = "mostrar_ayuda_ahorros";


        public const int TAB_AHORROS = 0;
        public const int TAB_GASTOS = 1;

        public const int PASO_PRESUPUESTO = 1;
        public const int PASO_INGRESOS = 2;
        public const int PASO_GASTOS = 3;


        public const int TIPO_INGRESO = 1001;
        public const int TIPO_GASTO = 1002;
        public const int TIPO_AHORRO = 1003;
        public const int TIPO_CREDITO = 1004;

        public const int TIPO_PROYECTADO = 1;
        public const int TIPO_REAL = 2;
        public const int TIPO_DIFERENCIA = 3;
    }
}
