﻿using System;
using System.Collections.Generic;

namespace Proifed.Config
{
    public class DataRepositoryUtil
    {

        public static int DiferenciaMeses(DateTime fechaInicio, DateTime fechaFin)
        {
            int diferenciaMeses = ((fechaFin.Year * 12) + fechaFin.Month) - ((fechaInicio.Year * 12) + fechaInicio.Month);
            return diferenciaMeses;
        }

        public static List<Mes> obtMeses(){
            List<Mes> meses = new List<Mes>(12);
            meses.Add(new Mes(1, "Enero"));
            meses.Add(new Mes(2, "Febrero"));
            meses.Add(new Mes(3, "Marzo"));
            meses.Add(new Mes(4, "Abril"));
            meses.Add(new Mes(5, "Mayo"));
            meses.Add(new Mes(6, "Junio"));
            meses.Add(new Mes(7, "Julio"));
            meses.Add(new Mes(8, "Agosto"));
            meses.Add(new Mes(9, "Septiembre"));
            meses.Add(new Mes(10, "Octubre"));
            meses.Add(new Mes(11, "Noviembre"));
            meses.Add(new Mes(12, "Diciembre"));
            return meses;

        }

        public static List<int> obtAnnos(){
            int annoActual = DateTime.Now.Year;
            List<int> annos = new List<int>(20);
            annos.Add(annoActual);
            for (int i = 1; i < 20; i++){
                annos.Add(annoActual + i);
            }
            return annos;
        }

        public static List<RubroIngreso> obtTiposIngreso(){
            List<RubroIngreso> rubros = new List<RubroIngreso>(4);
            rubros.Add(new RubroIngreso(0, "Salario"));
            rubros.Add(new RubroIngreso(1, "Alquiler"));
            rubros.Add(new RubroIngreso(2, "Pensión"));
            rubros.Add(new RubroIngreso(3, "Otros"));
            return rubros;
        }

        public static List<Plazo> obtPlazos()
        {
            List<Plazo> plazos = new List<Plazo>(4);
            plazos.Add(new Plazo(1));
            plazos.Add(new Plazo(3));
            plazos.Add(new Plazo(6));
            plazos.Add(new Plazo(12));
            return plazos;
        }

        public static List<PlazoAhorro> obtPlazosAhorro()
        {
            List<PlazoAhorro> plazos = new List<PlazoAhorro>(6);
            plazos.Add(new PlazoAhorro(1));
            plazos.Add(new PlazoAhorro(1.5));
            plazos.Add(new PlazoAhorro(2));
            plazos.Add(new PlazoAhorro(3));
            plazos.Add(new PlazoAhorro(4));
            plazos.Add(new PlazoAhorro(5));
            return plazos;
        }

        public static List<RubroGasto> obtRubrosGasto(){
            List<RubroGasto> gastos = new List<RubroGasto>(11);
            gastos.Add(new RubroGasto(1, "Gastos del hogar", new List<DetalleGasto>
            {
                new DetalleGasto(1, "Alimentación"),
                new DetalleGasto(2, "Limpieza, mantenimiento"),
                new DetalleGasto(3, "Mascotas"),
                new DetalleGasto(4, "Articulos del hogar"),
                new DetalleGasto(5, "Otros")
            }));
            gastos.Add(new RubroGasto(2, "Transportes", new List<DetalleGasto>
            {
                new DetalleGasto(1, "Combustible"),
                new DetalleGasto(2, "Mantenimiento de vehiculos"),
                new DetalleGasto(3, "Reparaciones de vehiculos"),
                new DetalleGasto(4, "Seguro"),
                new DetalleGasto(5, "Pago de peajes"),
                new DetalleGasto(6, "Pasajes de bus"),
                new DetalleGasto(7, "Otros")
            }));
            gastos.Add(new RubroGasto(3, "Vivienda", new List<DetalleGasto>
            {
                new DetalleGasto(1, "Pago de alquiler"),
                new DetalleGasto(2, "Impuestos"),
                new DetalleGasto(3, "Vigilancia"),
                new DetalleGasto(4, "Mantenimiento"),
                new DetalleGasto(5, "Seguro de incendios u otro"),
                new DetalleGasto(6, "Otros")
            }));
            gastos.Add(new RubroGasto(4, "Servicios públicos y otros", new List<DetalleGasto>
            {
                new DetalleGasto(1, "Electricidad"),
                new DetalleGasto(2, "Agua"),
                new DetalleGasto(3, "Telefono residencial"),
                new DetalleGasto(4, "Teléfonos celulares"),
                new DetalleGasto(5, "Internet"),
                new DetalleGasto(6, "Recolección de basura"),
                new DetalleGasto(7, "Otros")
            }));
            gastos.Add(new RubroGasto(5, "Salud", new List<DetalleGasto>
            {
                new DetalleGasto(1, "Consultas médicas"),
                new DetalleGasto(2, "Medicamentos"),
                new DetalleGasto(3, "Cuidado personal"),
                new DetalleGasto(4, "Seguro médico privado"),
                new DetalleGasto(5, "Seguros de vida u otros"),
                new DetalleGasto(6, "Otros")
            }));
            gastos.Add(new RubroGasto(6, "Educación y cuido", new List<DetalleGasto>
            {
                new DetalleGasto(1, "Educación adultos"),
                new DetalleGasto(2, "Educación niños"),
                new DetalleGasto(3, "Charlas o congresos"),
                new DetalleGasto(4, "Cuido"),
                new DetalleGasto(5, "Gimnasio"),
                new DetalleGasto(6, "Actividades(música, ballet, etc"),
                new DetalleGasto(7, "Otros")
            }));
            gastos.Add(new RubroGasto(7, "Obligaciones", new List<DetalleGasto>
            {
                new DetalleGasto(1, "Crédito hipotecario"),
                new DetalleGasto(2, "Crédito vehículo"),
                new DetalleGasto(3, "Créditos personales"),
                new DetalleGasto(4, "Tarjetas de crédito"),
                new DetalleGasto(5, "Otros")
            }));
            gastos.Add(new RubroGasto(8, "Ahorro", new List<DetalleGasto>
            {
                new DetalleGasto(1, "Fondo de presión voluntaria"),
                new DetalleGasto(2, "Ahorro para emergencias del mes"),
                new DetalleGasto(3, "Ahorro para \"mis metas\""),
                new DetalleGasto(4, "Para inversiones"),
                new DetalleGasto(5, "Otros")
            }));
            gastos.Add(new RubroGasto(9, "Entretenimiento", new List<DetalleGasto>
            {
                new DetalleGasto(1, "TV por cable"),
                new DetalleGasto(2, "Restaurantes"),
                new DetalleGasto(3, "Vacaciones"),
                new DetalleGasto(4, "Salidas nocturnas, cine, etc"),
                new DetalleGasto(5, "Otros")
            }));
            gastos.Add(new RubroGasto(10, "Solidaridad", new List<DetalleGasto>
            {
                new DetalleGasto(1, "Ayuda a familiares"),
                new DetalleGasto(2, "Donaciones"),
                new DetalleGasto(3, "Otros")
            }));
            gastos.Add(new RubroGasto(11, "Otros", new List<DetalleGasto>
            {
                new DetalleGasto(1, "Otros")
            }));

            return gastos;
        }

        public static List<TipoPrestamo> obtTiposPrestamo(){
            List<TipoPrestamo> tipos = new List<TipoPrestamo>(3);
            tipos.Add(new TipoPrestamo(1, "Vivienda"));
            tipos.Add(new TipoPrestamo(2, "Vehículo"));
            tipos.Add(new TipoPrestamo(3, "Personal"));
            tipos.Add(new TipoPrestamo(4, "Otros"));
            return tipos;
        }

    }

    public class Mes{

        public int Id { get; set; }
        public string Nombre { get; set; }

        public Mes(int id, string nombre){
            this.Id = id;
            this.Nombre = nombre;
        }

        public override string ToString()
        {
            return Nombre;
        }

    }

    public class Plazo{
        public int Cantidad { get; set; }

        public Plazo(int cantidad){
            this.Cantidad = cantidad;
        }

        public override string ToString()
        {
            return Cantidad + " Meses";
        }
    }

    public class RubroIngreso{
        public int Id { get; set; }
        public string Nombre { get; set; }

        public RubroIngreso(int id, string nombre){
            this.Id = id;
            this.Nombre = nombre;
        }

        public override string ToString()
        {
            return Nombre;
        }
    }

    public class RubroGasto{
        public int Id { get; set; }
        public string Nombre { get; set; }
        public List<DetalleGasto> Gastos { get; set; }

        public RubroGasto(int id, string nombre, List<DetalleGasto> gastos){
            this.Id = id;
            this.Nombre = nombre;
            this.Gastos = gastos;
        }

        public override string ToString()
        {
            return Nombre;
        }

    }

    public class DetalleGasto{
        public int Id { get; set; }
        public string Nombre { get; set; }

        public DetalleGasto(int id, string nombre){
            this.Id = id;
            this.Nombre = nombre;
        }

        public override string ToString()
        {
            return Nombre;
        }
    }

    public class TipoPrestamo{
        public int Id { get; set; }
        public string Descripcion { get; set; }

        public TipoPrestamo(int id, string descripcion){
            this.Id = id;
            this.Descripcion = descripcion;
        }

        public override string ToString()
        {
            return Descripcion;
        }

    }

    public class PlazoAhorro
    {
        public double CantidadAnnos { get; set; }
        public int CantidadMeses { get; set; }
        public double InteresAnual { get; set; }
        public double InteresMensual { get; set; }

        public PlazoAhorro(double cantidadAnnos)
        {
            this.CantidadAnnos = cantidadAnnos;
            this.CantidadMeses = (int)(CantidadAnnos * 12);
            switch (CantidadMeses)
            {
                case 12:
                    InteresAnual = 7.61;
                    InteresMensual = 0.00634;
                    break;
                case 18:
                    InteresAnual = 7.70;
                    InteresMensual = 0.00642;
                    break;
                case 24:
                    InteresAnual = 7.83;
                    InteresMensual = 0.00653;
                    break;
                case 36:
                    InteresAnual = 7.96;
                    InteresMensual = 0.00663;
                    break;
                case 48:
                    InteresAnual = 8.07;
                    InteresMensual = 0.00673;
                    break;
                case 60:
                    InteresAnual = 8.23;
                    InteresMensual = 0.00686;
                    break;
            }
        }

        public override string ToString()
        {
            return CantidadAnnos + " Años";
        }
    }

}
