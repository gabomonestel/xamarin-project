﻿using Newtonsoft.Json;
using Proifed.Config;
using Proifed.Data;
using Proifed.Models;
using Proifed.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Proifed.ViewModels
{
    public class AgregarIngresoProyectadoViewModel
    {
        private IRepository<IngresoProyectado> IngresoProyectadoRepo;
        private IRepository<Ingreso> IngresosRepo;
        public PresupuestoViewModel PresupuestoViewModel { get; private set; }
        public IngresoProyectado Ingreso { get; set; }

        public AgregarIngresoProyectadoViewModel(ISQLiteConnection connectionService)
        {
            InitIngresoProyectadoRepo(connectionService);
            InitRepo(connectionService);
            PresupuestoViewModel = new PresupuestoViewModel();
        }

        public AgregarIngresoProyectadoViewModel(ISQLiteConnection connectionService, IngresoProyectado ingreso)
        {
            InitIngresoProyectadoRepo(connectionService);
            InitRepo(connectionService);
            PresupuestoViewModel = new PresupuestoViewModel();
            this.Ingreso = ingreso;
        }


        public void InitIngresoProyectadoRepo(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                IngresoProyectadoRepo = new Repository<IngresoProyectado>(connectionService);
            }
        }

        public void InitRepo(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                IngresosRepo = new Repository<Ingreso>(connectionService);
            }
        }


        public async Task<int> GuardarIngresoProyectado(IngresoProyectado ingreso, int plazo)
        {
            bool creando = ingreso.Id == 0;

            int guardado = 0;
            DateTime fechaActual = DateUtil.FechaFinMes();
            if (creando)
            {
                ingreso.FechaUltimaGeneracion = fechaActual;
                guardado = await IngresoProyectadoRepo.Insert(ingreso);


                if (guardado != 0)
                {
                    //Guarda los datos reales futuros
                    for (int i = 0; i < plazo; i++)
                    {
                        Ingreso lIngresoReal = new Ingreso();
                        lIngresoReal = CopyUtil.CopiarIngresoProyectado(ingreso, lIngresoReal, true);
                        lIngresoReal.FechaCreacion = lIngresoReal.FechaCreacion.AddMonths(i);
                        guardado = await GuardarIngreso(lIngresoReal);
                        Ingreso lIngresProyectado = new Ingreso();
                        lIngresProyectado = CopyUtil.CopiarIngresoProyectado(ingreso, lIngresProyectado, false);
                        lIngresProyectado.FechaCreacion = lIngresProyectado.FechaCreacion.AddMonths(i);
                        guardado = await GuardarIngreso(lIngresProyectado);
                    }
                }

            }
            else
            {
                await IngresoProyectadoRepo.Update(ingreso);

                //Borra los gastos reales futuros previamente generados
                int borrados = await IngresosRepo.QuerableTable()
                                                 .Where(i => i.IdProyectado == ingreso.Id
                                                        //&& i.EsReal == true
                                                        && i.ModificadoUsuario == false
                                                        && i.FechaCreacion >= fechaActual)
                                                 .DeleteAsync();


                //Guarda los nuevos datos reales futuros
                for (int i = 0; i < plazo; i++)
                {
                    Ingreso lIngresoReal = new Ingreso();
                    lIngresoReal = CopyUtil.CopiarIngresoProyectado(ingreso, lIngresoReal, true);
                    lIngresoReal.FechaCreacion = lIngresoReal.FechaCreacion.AddMonths(i);
                    guardado = await GuardarIngreso(lIngresoReal);
                    Ingreso lIngresProyectado = new Ingreso();
                    lIngresProyectado = CopyUtil.CopiarIngresoProyectado(ingreso, lIngresProyectado, false);
                    lIngresProyectado.FechaCreacion = lIngresProyectado.FechaCreacion.AddMonths(i);
                    guardado = await GuardarIngreso(lIngresProyectado);
                }

            }

            return guardado;
        }


        private async Task<int> GuardarIngreso(Ingreso ingreso)
        {
            if (ingreso.Id > 0)
            {
                return await IngresosRepo.Update(ingreso);
            }
            else
            {
                return await IngresosRepo.Insert(ingreso);
            }
        }

    }
}
