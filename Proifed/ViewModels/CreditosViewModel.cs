﻿using Proifed.Data;
using Proifed.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Proifed.ViewModels
{
    public class CreditosViewModel
    {
        private IRepository<Credito> CreditoRepo;
        public ObservableCollection<Credito> CreditosData { get; private set; } = new ObservableCollection<Credito>();
        public PresupuestoViewModel PresupuestoViewModel { get; private set; }
        public double TotalMensual { get; set; }

        public CreditosViewModel(ISQLiteConnection connectionService)
        {
            InitRepo(connectionService);
            PresupuestoViewModel = new PresupuestoViewModel();
        }

        public void InitRepo(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                CreditoRepo = new Repository<Credito>(connectionService);
            }
        }

        public async Task RefreshData()
        {
            TotalMensual = 0;
            CreditosData.Clear();
            //List<Credito> data = await CreditoRepo.GetAllAsync();
            string query = "SELECT * FROM Credito WHERE FechaFinalizacion >= ':fechaActual'";// AND FechaInicio <= ':fechaActual'";
            query = query.Replace(":fechaActual", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).Ticks.ToString());
            List<Credito> data = await CreditoRepo.Query(query);
            data.ForEach(x =>
            {
                CreditosData.Add(x);
                TotalMensual += x.CuotaMensual;
            });

            Debug.WriteLine("lista de metas");
        }

        public async Task<int> DeleteCredito(Credito credito)
        {

            int borrado = await CreditoRepo.Delete(credito);
            /*if (borrado > 0)
            {
                DateTime fechaActual = DateUtil.FechaFinMes();
                List<Ahorro> lAhorros = await AhorrosRepo.QuerableTable()
                                                 .Where(i => i.IdProyectado == ahorro.Id)
                                                 .ToListAsync();
                lAhorros.ForEach(async x =>
                {
                    await DeleteAhorro(x);
                });
                return 1;
            }
            else
            {
                return 0;
            }
            */
            return borrado;           
        }

    }
}
