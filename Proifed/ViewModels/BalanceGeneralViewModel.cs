﻿using Newtonsoft.Json;
using Proifed.Config;
using Proifed.Data;
using Proifed.Models;
using Proifed.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Proifed.ViewModels
{
    public class BalanceGeneralViewModel
    {
        private IRepository<Ingreso> IngresosRepo;
        private IRepository<Gasto> GastosRepo;
        private IRepository<Ahorro> AhorrosRepo;
        private IRepository<Credito> CreditosRepo;
        //private IRepository<Ahorro> AhorrosRepo;
        public PresupuestoViewModel PresupuestoViewModel { get; private set; }
        public ObservableCollection<BalanceItem> Items { get; private set; } = new ObservableCollection<BalanceItem>();
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public double TotalReal { get; set; }

        public BalanceGeneralViewModel(ISQLiteConnection connectionService)
        {
            InitRepos(connectionService);
            PresupuestoViewModel = new PresupuestoViewModel();
            DateTime fechaFin = DateUtil.FechaFinMes();
            this.FechaInicio = fechaFin.AddMonths(-1).AddDays(1);
            this.FechaFin = fechaFin;
        }

        public void InitRepos(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                IngresosRepo = new Repository<Ingreso>(connectionService);
                GastosRepo = new Repository<Gasto>(connectionService);
                CreditosRepo = new Repository<Credito>(connectionService);
                AhorrosRepo = new Repository<Ahorro>(connectionService);
            }
        }

        public async Task CargarBalance()
        {
            this.Items.Clear();
            double totalIngresos = await TotalIngresosReales();
            double totalGastos = await TotalGastosReales();
            double totalAhorros = await TotalAhorros();
            double totalCreditos = await TotalCreditos();
            double diferencia = totalIngresos - totalGastos - totalAhorros - totalCreditos;
            string colorDiferencia = "#008000";

            if (diferencia < 0)
            {
                colorDiferencia = "#ff0000";
            }

            this.Items.Add(new BalanceItem(Constants.TIPO_INGRESO, "Ingresos Reales", totalIngresos, "#000000"));
            this.Items.Add(new BalanceItem(Constants.TIPO_GASTO, "Gastos Reales", totalGastos, "#000000"));
            this.Items.Add(new BalanceItem(Constants.TIPO_AHORRO, "Ahorros", totalAhorros, "#000000"));
            this.Items.Add(new BalanceItem(Constants.TIPO_CREDITO, "Creditos", totalCreditos, "#000000"));
            this.Items.Add(new BalanceItem(Constants.TIPO_DIFERENCIA, "Diferencia", diferencia, colorDiferencia));

        }

        public async Task<double> TotalIngresosReales()
        {
            List<Ingreso> data = await IngresosRepo.QuerableTable()
                                               .Where(i => i.EsReal == true
                                                          && i.FechaCreacion >= FechaInicio
                                                          && i.FechaCreacion <= FechaFin)
                                                   .ToListAsync();
            double totalProyectado = 0;
            data.ForEach(x => totalProyectado += x.Monto);
            return totalProyectado;
        }

        public async Task<double> TotalGastosReales()
        {
            List<Gasto> data = await GastosRepo.QuerableTable()
                                               .Where(i => i.EsReal == true
                                                          && i.FechaCreacion >= FechaInicio
                                                          && i.FechaCreacion <= FechaFin)
                                                   .ToListAsync();
            double totalProyectado = 0;
            data.ForEach(x => totalProyectado += x.Monto);
            return totalProyectado;
        }

        public async Task<double> TotalCreditos()
        {
            DateTime fechaActual = DateUtil.FechaFinMes();
            List<Credito> data = await CreditosRepo.QuerableTable()
                                                   .Where(i => i.FechaInicio <= fechaActual
                                                          && i.FechaFinalizacion >= fechaActual)
                                                   .ToListAsync();
            double totalCreditos = 0;
            data.ForEach(x => totalCreditos += x.CuotaMensual);
            return totalCreditos;
        }

        public async Task<double> TotalAhorros()
        {
            DateTime fechaActual = DateUtil.FechaFinMes();
            List<Ahorro> data = await AhorrosRepo.QuerableTable()
                                               .Where(i => i.FechaCreacion >= FechaInicio
                                                          && i.FechaCreacion <= FechaFin)
                                                   .ToListAsync();
            double totalAhorros = 0;
            data.ForEach(x => totalAhorros += x.Monto);
            return totalAhorros;
        }

    }
}
