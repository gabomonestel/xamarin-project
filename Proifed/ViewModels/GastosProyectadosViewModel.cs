﻿using Proifed.Data;
using Proifed.Models;
using Proifed.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Proifed.ViewModels
{
    public class GastosProyectadosViewModel
    {
        private IRepository<Gasto> GastosRepo;
        private IRepository<GastoProyectado> GastoProyectadoRepo;
        public ObservableCollection<GastoProyectado> GastosData { get; private set; } = new ObservableCollection<GastoProyectado>();

        public PresupuestoViewModel PresupuestoViewModel { get; private set; }
        public double TotalProyectado { get; internal set; }

        public GastosProyectadosViewModel(ISQLiteConnection connectionService)
        {
            InitGastoRepo(connectionService);
            InitGastoProyectadoRepo(connectionService);
            PresupuestoViewModel = new PresupuestoViewModel();
        }

        public void InitGastoRepo(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                GastosRepo = new Repository<Gasto>(connectionService);
            }
        }

        public void InitGastoProyectadoRepo(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                GastoProyectadoRepo = new Repository<GastoProyectado>(connectionService);
            }
        }

        public async Task RefreshData()
        {
            TotalProyectado = 0;
            GastosData.Clear();
            DateTime fechaActual = DateUtil.FechaFinMes();
            List<GastoProyectado> data = await GastoProyectadoRepo.QuerableTable()
                                                            .Where(i => i.FechaCreacion <= fechaActual
                                                                   && i.FechaFinalizacion >= fechaActual)
                                                            .ToListAsync();
            data.ForEach(x => { GastosData.Add(x); TotalProyectado += x.Monto; });
        }

        public async Task<int> DeleteGastoProyectado(GastoProyectado gasto)
        {
            int borrado = await GastoProyectadoRepo.Delete(gasto);
            if (borrado > 0)
            {
                DateTime fechaActual = DateUtil.FechaFinMes();
                int borrados = await GastosRepo.QuerableTable()
                                                 .Where(i => i.IdProyectado == gasto.Id
                                                        && i.EsReal == true
                                                        && i.ModificadoUsuario == false
                                                        && i.FechaCreacion >= fechaActual)
                                                 .DeleteAsync();
                return borrado;
            }
            else
            {
                return 0;
            }

        }

        public async Task<int> DeleteGasto(Gasto gasto)
        {
            if (gasto.Id > 0)
            {
                return await GastosRepo.Delete(gasto);
            }
            else
            {
                return 1;
            }
        }

    }
}
