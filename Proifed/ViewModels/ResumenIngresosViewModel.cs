﻿using Newtonsoft.Json;
using Proifed.Config;
using Proifed.Data;
using Proifed.Models;
using Proifed.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Essentials;
using System.Linq;

namespace Proifed.ViewModels
{
    public class ResumenIngresosViewModel
    {
        private IRepository<Ingreso> IngresosRepo;
        public ObservableCollection<GrupoItem<DateTime, Ingreso>> Ingresos { get; private set; } = new ObservableCollection<GrupoItem<DateTime, Ingreso>>();
        public int Tipo { get; private set; }
        public DateTime FechaInicio { get; private set; }
        public DateTime FechaFin { get; private set; }


        public ResumenIngresosViewModel(ISQLiteConnection connectionService, int tipo, DateTime fechaInicio, DateTime fechaFin)
        {
            InitIngresoRepo(connectionService);
            this.Tipo = tipo;
            this.FechaInicio = fechaInicio;
            this.FechaFin = fechaFin;
        }

        public void InitIngresoRepo(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                IngresosRepo = new Repository<Ingreso>(connectionService);
            }
        }

        public async Task CargarDatos()
        {
            this.Ingresos.Clear();
            bool esReal = Tipo == Constants.TIPO_REAL ? true : false;
            IEnumerable<Ingreso> data = await IngresosRepo.QuerableTable()
                                                   .Where(i => i.EsReal == esReal
                                                   && i.FechaCreacion >= FechaInicio
                                                   && i.FechaCreacion <= FechaFin)
                                                   .OrderByDescending(i => i.FechaCreacion)
                                                   .ToListAsync();

            var ordenado =  from ingreso in data 
                            orderby ingreso.FechaCreacion 
                            group ingreso by ingreso.FechaCreacion into grupoIngresos
                            select new GrupoItem<DateTime, Ingreso>(grupoIngresos.Key, grupoIngresos);

            new ObservableCollection<GrupoItem<DateTime, Ingreso>>(ordenado).ToList()
            .ForEach(x => Ingresos.Add(x));

        }

    }
}
