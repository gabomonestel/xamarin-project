﻿using Newtonsoft.Json;
using Proifed.Config;
using Proifed.Data;
using Proifed.Models;
using Proifed.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Proifed.ViewModels
{
    public class AgregarAhorroViewModel
    {
        private IRepository<AhorroProyectado> AhorroProyectadoRepo;
        private IRepository<Ahorro> AhorroRepo;
        public PresupuestoViewModel PresupuestoViewModel { get; private set; }
        public AhorroProyectado Ahorro { get; set; }

        public AgregarAhorroViewModel(ISQLiteConnection connectionService)
        {
            InitRepo(connectionService);
            PresupuestoViewModel = new PresupuestoViewModel();
        }

        public AgregarAhorroViewModel(ISQLiteConnection connectionService, AhorroProyectado ahorro)
        {
            InitRepo(connectionService);
            PresupuestoViewModel = new PresupuestoViewModel();
            this.Ahorro = ahorro;
        }

        public void InitRepo(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                AhorroProyectadoRepo = new Repository<AhorroProyectado>(connectionService);
                AhorroRepo = new Repository<Ahorro>(connectionService);
            }
        }

        public async Task<int> GuardarAhorroProyectado(AhorroProyectado ahorro, int plazo)
        {
            bool creando = ahorro.Id == 0;

            int guardado = 0;

            DateTime fechaActual = DateUtil.FechaFinMes();
            if (creando)//ahorro.Id > 0)
            {
                ahorro.FechaUltimaGeneracion = fechaActual;
                guardado = await AhorroProyectadoRepo.Insert(ahorro);


                if (guardado != 0)
                {
                    Ahorro lAhorroReal = new Ahorro();
                    lAhorroReal = CopyUtil.CopiarAhorroProyectado(ahorro, lAhorroReal);
                    lAhorroReal.Descripcion = "Aporte Inicial";
                    lAhorroReal.Monto = ahorro.AporteInicial;
                    guardado = await GuardarAhorro(lAhorroReal);

                    //Guarda los datos reales futuros
                    for (int i = 1; i <= plazo; i++)
                    {
                        Ahorro lAhorro = new Ahorro();
                        lAhorro = CopyUtil.CopiarAhorroProyectado(ahorro, lAhorro);
                        lAhorro.FechaCreacion = lAhorro.FechaCreacion.AddMonths(i);
                        lAhorro.Descripcion = "Mensualidad";
                        await GuardarAhorro(lAhorro);
                    }
                }
            }
            else
            {
                await AhorroProyectadoRepo.Update(ahorro);
                int borrados = await AhorroRepo.QuerableTable()
                                                 .Where(i => i.IdProyectado == ahorro.Id)
                                                 .DeleteAsync();

                Ahorro lAhorroReal = new Ahorro();
                lAhorroReal = CopyUtil.CopiarAhorroProyectado(ahorro, lAhorroReal);
                lAhorroReal.Descripcion = "Aporte Inicial";
                lAhorroReal.Monto = ahorro.AporteInicial;
                guardado = await GuardarAhorro(lAhorroReal);

                //Guarda los nuevos datos reales futuros
                for (int i = 1; i <= plazo; i++)
                {
                    Ahorro lAhorro = new Ahorro();
                    lAhorro = CopyUtil.CopiarAhorroProyectado(ahorro, lAhorro);
                    lAhorro.FechaCreacion = lAhorro.FechaCreacion.AddMonths(i);
                    lAhorro.Descripcion = "Mensualidad";
                    await GuardarAhorro(lAhorro);
                }


            }

            return guardado;
        }


        public async Task<int> GuardarAhorro(Ahorro ahorro)
        {
            if (ahorro.Id > 0)
            {
                return await AhorroRepo.Update(ahorro);
            }
            else
            {
                return await AhorroRepo.Insert(ahorro);
            }
        }

    }
}
