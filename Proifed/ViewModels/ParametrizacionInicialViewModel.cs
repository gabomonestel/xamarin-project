﻿using System;
using Proifed.Config;
using Proifed.Models;
using Xamarin.Essentials;
using Newtonsoft.Json;
using Xamarin.Forms;
using Proifed.Data;
using System.Threading.Tasks;

namespace Proifed.ViewModels
{
    public class ParametrizacionInicialViewModel
    {
        public PresupuestoViewModel PresupuestoViewModel { get; private set; }
        public bool Configurado { get; set; }
        public bool Configurando { get; set; }

        public ParametrizacionInicialViewModel(ISQLiteConnection connectionService)
        {
            this.PresupuestoViewModel = new PresupuestoViewModel();
        }


    }
}
