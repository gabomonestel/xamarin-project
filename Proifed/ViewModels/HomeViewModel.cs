﻿using Newtonsoft.Json;
using Proifed.Config;
using Proifed.Data;
using Proifed.Models;
using Proifed.Util;
using System;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Proifed.ViewModels
{
    public class HomeViewModel
    {
        private IRepository<IngresoProyectado> IngresosProyectadosRepo;
        private IRepository<Ingreso> IngresosRepo;
        private IRepository<GastoProyectado> GastosProyectadosRepo;
        private IRepository<Gasto> GastosRepo;
        private IRepository<AhorroProyectado> AhorroProyectadoRepo;
        private IRepository<Ahorro> AhorroRepo;
        private IRepository<Credito> CreditoRepo;

        public int TotalIngresosProyectado { get; private set; }
        public PresupuestoViewModel PresupuestoViewModel { get; private set; }

        public HomeViewModel(ISQLiteConnection connectionService)
        {
            InitRepo(connectionService);
            PresupuestoViewModel = new PresupuestoViewModel();
            RefreshData();
        }

        public void InitRepo(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                IngresosProyectadosRepo = new Repository<IngresoProyectado>(connectionService);
                IngresosRepo = new Repository<Ingreso>(connectionService);
                GastosProyectadosRepo = new Repository<GastoProyectado>(connectionService);
                GastosRepo = new Repository<Gasto>(connectionService);
                AhorroProyectadoRepo = new Repository<AhorroProyectado>(connectionService);
                AhorroRepo = new Repository<Ahorro>(connectionService);
                CreditoRepo = new Repository<Credito>(connectionService);
            }
        }


        public async void RefreshData()
        {
            var ingresos = IngresosProyectadosRepo.QuerableTable();
            DateTime fechaActual = DateUtil.FechaFinMes();
            TotalIngresosProyectado = await ingresos.Where(i => i.FechaCreacion <= fechaActual
                                                           && i.FechaFinalizacion >= fechaActual).CountAsync();
            this.PresupuestoViewModel.CargarPresupuesto();
        }

        public async Task<int> BorrarPresupuesto() {
            int borrados = 0;

            borrados += await IngresosProyectadosRepo.DeleteAll();
            borrados += await IngresosRepo.DeleteAll();
            borrados += await GastosProyectadosRepo.DeleteAll();
            borrados += await GastosRepo.DeleteAll();
            borrados += await AhorroProyectadoRepo.DeleteAll();
            borrados += await AhorroRepo.DeleteAll();
            borrados += await CreditoRepo.DeleteAll();
            Preferences.Set(Constants.PRESUPUESTO, "");

            return borrados;
        }

    }
}
