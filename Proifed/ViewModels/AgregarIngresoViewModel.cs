﻿using Newtonsoft.Json;
using Proifed.Config;
using Proifed.Data;
using Proifed.Models;
using Proifed.Util;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Proifed.ViewModels
{
    public class AgregarIngresoViewModel
    {
        private IRepository<Ingreso> IngresosRepo;
        public PresupuestoViewModel PresupuestoViewModel { get; private set; }
        public Ingreso Ingreso { get; set; }

        public AgregarIngresoViewModel(ISQLiteConnection connectionService)
        {
            InitRepo(connectionService);
            PresupuestoViewModel = new PresupuestoViewModel();
        }

        public AgregarIngresoViewModel(ISQLiteConnection connectionService, Ingreso ingreso)
        {
            InitRepo(connectionService);
            PresupuestoViewModel = new PresupuestoViewModel();
            this.Ingreso = ingreso;
        }

        public void InitRepo(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                IngresosRepo = new Repository<Ingreso>(connectionService);
            }
        }


        public async Task<int> GuardarIngreso(Ingreso ingreso)
        {
            if (ingreso.Id > 0)
            {
                return await IngresosRepo.Update(ingreso);
            }
            else
            {
                return await IngresosRepo.Insert(ingreso);
            }
        }

    }
}
