﻿using Newtonsoft.Json;
using Proifed.Config;
using Proifed.Data;
using Proifed.Models;
using Proifed.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Essentials;
using System.Linq;

namespace Proifed.ViewModels
{
    public class ResumenAhorrosViewModel
    {
        private IRepository<Ahorro> AhorrosRepo;
        public ObservableCollection<GrupoItem<DateTime, Ahorro>> Ahorros { get; private set; } = new ObservableCollection<GrupoItem<DateTime, Ahorro>>();
        public DateTime FechaInicio { get; private set; }
        public DateTime FechaFin { get; private set; }


        public ResumenAhorrosViewModel(ISQLiteConnection connectionService, DateTime fechaInicio, DateTime fechaFin)
        {
            InitIngresoRepo(connectionService);
            this.FechaInicio = fechaInicio;
            this.FechaFin = fechaFin;
        }

        public void InitIngresoRepo(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                AhorrosRepo = new Repository<Ahorro>(connectionService);
            }
        }

        public async Task CargarDatos()
        {
            this.Ahorros.Clear();
            IEnumerable<Ahorro> data = await AhorrosRepo.QuerableTable()
                                                   .Where(i => i.FechaCreacion >= FechaInicio
                                                   && i.FechaCreacion <= FechaFin)
                                                   .OrderByDescending(i => i.FechaCreacion)
                                                   .ToListAsync();

            var ordenado =  from gasto in data 
                            orderby gasto.FechaCreacion 
                            group gasto by gasto.FechaCreacion into grupoAhorros
                            select new GrupoItem<DateTime, Ahorro>(grupoAhorros.Key, grupoAhorros);

            new ObservableCollection<GrupoItem<DateTime, Ahorro>>(ordenado).ToList()
            .ForEach(x => Ahorros.Add(x));

        }

    }
}
