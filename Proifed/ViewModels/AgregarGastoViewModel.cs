﻿using Newtonsoft.Json;
using Proifed.Config;
using Proifed.Data;
using Proifed.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Proifed.ViewModels
{
    public class AgregarGastoViewModel
    {
        private IRepository<Gasto> GastosRepo;
        public PresupuestoViewModel PresupuestoViewModel { get; private set; }
        public Gasto Gasto { get; set; }

        public AgregarGastoViewModel(ISQLiteConnection connectionService)
        {
            InitRepo(connectionService);
            PresupuestoViewModel = new PresupuestoViewModel();
        }

        public AgregarGastoViewModel(ISQLiteConnection connectionService, Gasto gasto)
        {
            InitRepo(connectionService);
            PresupuestoViewModel = new PresupuestoViewModel();
            this.Gasto = gasto;
        }

        public void InitRepo(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                GastosRepo = new Repository<Gasto>(connectionService);
            }
        }

        public async Task<int> GuardarGasto(Gasto gasto)
        {
            if (gasto.Id > 0)
            {
                return await GastosRepo.Update(gasto);
            }
            else
            {
                return await GastosRepo.Insert(gasto);
            }
        }

    }
}
