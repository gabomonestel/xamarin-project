﻿using Proifed.Data;
using Proifed.Models;
using Proifed.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Proifed.ViewModels
{
    public class AhorrosViewModel
    {
        private IRepository<Ahorro> AhorrosRepo;
        private IRepository<AhorroProyectado> AhorroProyectadoRepo;
        public ObservableCollection<AhorroProyectado> AhorrosData { get; private set; } = new ObservableCollection<AhorroProyectado>();
        public PresupuestoViewModel PresupuestoViewModel { get; private set; }
        public double TotalMensual { get; set; }

        public AhorrosViewModel(ISQLiteConnection connectionService)
        {
            InitRepo(connectionService);
            PresupuestoViewModel = new PresupuestoViewModel();
        }

        public void InitRepo(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                AhorroProyectadoRepo = new Repository<AhorroProyectado>(connectionService);
                AhorrosRepo = new Repository<Ahorro>(connectionService);
            }
        }

        public async Task RefreshData()
        {
            TotalMensual = 0;
            AhorrosData.Clear();
            DateTime fechaActual = DateUtil.FechaFinMes();
            List<AhorroProyectado> data = await AhorroProyectadoRepo.QuerableTable()
                                            .Where(i => i.FechaCreacion <= fechaActual
                                            && i.FechaFinalizacion >= fechaActual
                                            )
                                            .ToListAsync();
            data.ForEach(x =>
            {
                AhorrosData.Add(x);
                TotalMensual += x.Monto;
            });
        }

        public async Task<int> DeleteAhorro(AhorroProyectado ahorro)
        {

            int borrado = await AhorroProyectadoRepo.Delete(ahorro);
            if (borrado > 0)
            {
                DateTime fechaActual = DateUtil.FechaFinMes();
                List<Ahorro> lAhorros = await AhorrosRepo.QuerableTable()
                                                 .Where(i => i.IdProyectado == ahorro.Id)
                                                 .ToListAsync();
                lAhorros.ForEach(async x =>
                {
                    await DeleteAhorro(x);
                });
                return 1;
            }
            else
            {
                return 0;
            }
        }


        public async Task<int> DeleteAhorro(Ahorro ahorro)
        {
            if (ahorro.Id > 0)
            {
                return await AhorrosRepo.Delete(ahorro);
            }
            else
            {
                return 1;
            }
        }

        public async void CrearAhorrosMes()
        {
            DateTime fechaActual = DateUtil.FechaFinMes();
            if (PresupuestoViewModel.Presupuesto != null)
            {

                List<AhorroProyectado> gastosProyectados = await AhorroProyectadoRepo.QuerableTable()
                                                 .Where(i => i.FechaCreacion <= fechaActual
                                                        && i.FechaFinalizacion >= fechaActual
                                                        && i.FechaUltimaGeneracion != fechaActual)
                                                 .ToListAsync();


                gastosProyectados.ForEach(async ahorro =>
                {
                    Ahorro lAhorro = new Ahorro();
                    lAhorro = CopyUtil.CopiarAhorroProyectado(ahorro, lAhorro);
                    lAhorro.Descripcion = "Mensualidad";
                    await AhorrosRepo.Insert(lAhorro);

                    ahorro.FechaUltimaGeneracion = fechaActual;
                    await AhorroProyectadoRepo.Update(ahorro);

                });
                await RefreshData();
            }
        }

    }
}
