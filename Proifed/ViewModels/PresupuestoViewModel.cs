﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Proifed.Config;
using Proifed.Data;
using Proifed.Models;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Proifed.ViewModels
{
    public class PresupuestoViewModel
    {

        public Presupuesto Presupuesto { private set; get; }


        public PresupuestoViewModel()
        {
            //InitRepo(connectionService);
            CargarPresupuesto();
        }

        public void CargarPresupuesto()
        {
            Presupuesto = JsonConvert.DeserializeObject<Presupuesto>(Preferences.Get(Constants.PRESUPUESTO, ""));
        }

        public void GuardarPresupuesto(Presupuesto presupuesto)
        {
            Preferences.Set(Constants.PRESUPUESTO, JsonConvert.SerializeObject(presupuesto));
        }

    }
}
