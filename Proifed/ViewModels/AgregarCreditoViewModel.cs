﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Proifed.Config;
using Proifed.Data;
using Proifed.Models;
using Xamarin.Essentials;

namespace Proifed.ViewModels
{
    public class AgregarCreditoViewModel
    {
        private IRepository<Credito> CreditoRepo;
        public List<TipoPrestamo> Prestamos { get; private set; }
        public PresupuestoViewModel PresupuestoViewModel { get; private set; }
        public Credito Credito { get; set; }

        public AgregarCreditoViewModel(ISQLiteConnection connectionService)
        {
            InitRepo(connectionService);
            PresupuestoViewModel = new PresupuestoViewModel();
            Prestamos = DataRepositoryUtil.obtTiposPrestamo();
        }

        public AgregarCreditoViewModel(ISQLiteConnection connectionService, Credito credito)
        {
            this.Credito = credito;
            Prestamos = DataRepositoryUtil.obtTiposPrestamo();
        }

        public void InitRepo(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                CreditoRepo = new Repository<Credito>(connectionService);
            }
        }

        public Task<int> GuardarCredito(Credito credito)
        {
            if (credito.Id > 0)
            {
                return CreditoRepo.Update(credito);
            }
            else
            {
                return CreditoRepo.Insert(credito);
            }
        }
    }
}
