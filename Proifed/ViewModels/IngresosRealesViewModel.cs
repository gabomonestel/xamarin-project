﻿using Proifed.Data;
using Proifed.Models;
using Proifed.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Proifed.ViewModels
{
    public class IngresosRealesViewModel
    {
        private IRepository<Ingreso> IngresosRepo;
        private IRepository<IngresoProyectado> IngresoProyectadoRepo;
        public ObservableCollection<Ingreso> IngresosData { get; private set; } = new ObservableCollection<Ingreso>();
        public PresupuestoViewModel PresupuestoViewModel { get; private set; }
        public double TotalReal { get; set; }
        public DateTime Fecha { get; private set; }

        public IngresosRealesViewModel(ISQLiteConnection connectionService, DateTime fecha)
        {
            InitRepos(connectionService);
            this.PresupuestoViewModel = new PresupuestoViewModel();
            this.Fecha = fecha;
        }

        public void InitRepos(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                IngresosRepo = new Repository<Ingreso>(connectionService);
                IngresoProyectadoRepo = new Repository<IngresoProyectado>(connectionService);
            }
        }

        public double TotalIngresosProyectados()
        {
            DateTime fechaActual = DateUtil.FechaFinMes();
            List<Ingreso> data = IngresosRepo.QuerableTable().Where(i => i.EsReal == false
                                                && i.FechaCreacion == fechaActual
                                               ).ToListAsync().Result;
            double totalProyectado = 0;
            data.ForEach(x => totalProyectado += x.Monto);
            return totalProyectado;
        }

        public async Task RefreshData()
        {
            TotalReal = 0;
            IngresosData.Clear();
            DateTime fechaActual = DateUtil.FechaFinMes();
            List<Ingreso> data = await IngresosRepo.QuerableTable()
                                                   .Where(i => i.EsReal == true
                                                          && i.FechaCreacion == fechaActual)
                                                   .ToListAsync();
            data.ForEach(x =>
            {
                IngresosData.Add(x);
                TotalReal += x.Monto;
            });

        }

        public int DeleteIngreso(Ingreso ingreso)
        {
            return IngresosRepo.Delete(ingreso).Result;
        }

        public int DeleteAll(){
            try
            {
                return IngresosRepo.DeleteAll().Result;
            }catch(Exception ex){
                String message = ex.Message;  
            }
            return -1;
        }


        /*public async void CrearIngresosRealesMes()
        {
            DateTime fechaActual = DateUtil.FechaFinMes();
            if (PresupuestoViewModel.Presupuesto != null) 
            {

                List<IngresoProyectado> proyectados = await IngresoProyectadoRepo.QuerableTable()
                                                       .Where(i => i.FechaCreacion <= fechaActual
                                                              && i.FechaFinalizacion >= fechaActual
                                                              && i.FechaUltimaGeneracion != fechaActual)
                                                       .ToListAsync();

                proyectados.ForEach(async ingreso =>
                {
                    Ingreso lIngresoProyectado = new Ingreso();
                    lIngresoProyectado = CopyUtil.CopiarIngresoProyectado(ingreso, lIngresoProyectado, false);
                    await IngresosRepo.Insert(lIngresoProyectado);

                    Ingreso lIngresoReal = new Ingreso();
                    lIngresoReal = CopyUtil.CopiarIngresoProyectado(ingreso, lIngresoReal, true);
                    await IngresosRepo.Insert(lIngresoReal);

                    ingreso.FechaUltimaGeneracion = fechaActual;
                    await IngresoProyectadoRepo.Update(ingreso);
                });

                await RefreshData();
            }
        }*/

        public async Task<bool> IngresosFueronModificados() {
            DateTime fechaActual = DateUtil.FechaFinMes();
            List<Ingreso> data = await IngresosRepo.QuerableTable()
                                                   .Where(i => i.EsReal == true
                                                          && i.ModificadoUsuario == true
                                                          && i.FechaCreacion == fechaActual)
                                                   .ToListAsync();
           
            return data.Count > 0;
        }

    }
}
