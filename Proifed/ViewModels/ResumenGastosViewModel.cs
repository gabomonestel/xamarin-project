﻿using Newtonsoft.Json;
using Proifed.Config;
using Proifed.Data;
using Proifed.Models;
using Proifed.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Essentials;
using System.Linq;

namespace Proifed.ViewModels
{
    public class ResumenGastosViewModel
    {
        private IRepository<Gasto> GastosRepo;
        public ObservableCollection<GrupoItem<DateTime, Gasto>> Gastos { get; private set; } = new ObservableCollection<GrupoItem<DateTime, Gasto>>();
        public int Tipo { get; private set; }
        public DateTime FechaInicio { get; private set; }
        public DateTime FechaFin { get; private set; }


        public ResumenGastosViewModel(ISQLiteConnection connectionService, int tipo, DateTime fechaInicio, DateTime fechaFin)
        {
            InitIngresoRepo(connectionService);
            this.Tipo = tipo;
            this.FechaInicio = fechaInicio;
            this.FechaFin = fechaFin;
        }

        public void InitIngresoRepo(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                GastosRepo = new Repository<Gasto>(connectionService);
            }
        }

        public async Task CargarDatos()
        {
            this.Gastos.Clear();
            bool esReal = Tipo == Constants.TIPO_REAL ? true : false;
            IEnumerable<Gasto> data = await GastosRepo.QuerableTable()
                                                   .Where(i => i.EsReal == esReal
                                                   && i.FechaCreacion >= FechaInicio
                                                   && i.FechaCreacion <= FechaFin)
                                                   .OrderByDescending(i => i.FechaCreacion)
                                                   .ToListAsync();

            var ordenado =  from gasto in data 
                            orderby gasto.FechaCreacion 
                            group gasto by gasto.FechaCreacion into grupoGastos
                            select new GrupoItem<DateTime, Gasto>(grupoGastos.Key, grupoGastos);

            new ObservableCollection<GrupoItem<DateTime, Gasto>>(ordenado).ToList()
            .ForEach(x => Gastos.Add(x));

        }

    }
}
