﻿using System;
using Proifed.Data;
using Proifed.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Proifed;
using System.Diagnostics;

namespace Proifed.ViewModels
{
    public class AgregarMetaViewModel
    {
        private IRepository<Meta> MetaRepo;

        public AgregarMetaViewModel()
        {
        }

        public void InitRepo(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                MetaRepo = new Repository<Meta>(connectionService);
            }
        }

        public int AddMeta(string nombre, 
                           string planAccion, 
                           string descripcion,
                           int mes,
                           int anno)
        {
            Meta meta = new Meta
            {
                Nombre = nombre,
                PlanAccion = planAccion,
                Descripcion = descripcion,
                Finaliza = new DateTime(anno, mes, 1).AddMonths(1).AddDays(-1)
            };
            return MetaRepo.Insert(meta).Result;
        }


    }
}
