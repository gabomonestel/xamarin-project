﻿using Newtonsoft.Json;
using Proifed.Config;
using Proifed.Data;
using Proifed.Models;
using Proifed.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Proifed.ViewModels
{
    public class BalanceGastosViewModel
    {
        private IRepository<Gasto> GastosRepo;
        public PresupuestoViewModel PresupuestoViewModel { get; private set; }
        public ObservableCollection<BalanceItem> Items { get; private set; } = new ObservableCollection<BalanceItem>();
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }

        public BalanceGastosViewModel(ISQLiteConnection connectionService)
        {
            InitRepo(connectionService);
            PresupuestoViewModel = new PresupuestoViewModel();
            DateTime fechaFin = DateUtil.FechaFinMes();
            this.FechaInicio = fechaFin.AddMonths(-1).AddDays(1);
            this.FechaFin = fechaFin;
        }

        public void InitRepo(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                GastosRepo = new Repository<Gasto>(connectionService);
            }
        }

        public async Task CargarBalance()
        {
            this.Items.Clear();
            double totalProyectado = await TotalGastosProyectados();
            double totalReal = await TotalGastosReales();
            double diferencia = totalProyectado - totalReal;
            string colorDiferencia = "#008000";

            if (diferencia < 0)
            {
                colorDiferencia = "#ff0000";
            }

            this.Items.Add(new BalanceItem(Constants.TIPO_PROYECTADO, "Gastos Proyectados", totalProyectado, "#000000"));
            this.Items.Add(new BalanceItem(Constants.TIPO_REAL, "Gastos Reales", totalReal, "#000000"));
            this.Items.Add(new BalanceItem(Constants.TIPO_DIFERENCIA, "Diferencia", diferencia, colorDiferencia));

        }

        public async Task<double> TotalGastosProyectados()
        {
            List<Gasto> data = await GastosRepo.QuerableTable()
                                                   .Where(i => i.EsReal == false
                                                          && i.FechaCreacion >= FechaInicio
                                                          && i.FechaCreacion <= FechaFin)
                                                   .ToListAsync();
            double totalProyectado = 0;
            data.ForEach(x => totalProyectado += x.Monto);
            return totalProyectado;
        }

        public async Task<double> TotalGastosReales()
        {
            List<Gasto> data = await GastosRepo.QuerableTable()
                                               .Where(i => i.EsReal == true
                                                          && i.FechaCreacion >= FechaInicio
                                                          && i.FechaCreacion <= FechaFin)
                                                   .ToListAsync();
            double totalProyectado = 0;
            data.ForEach(x => totalProyectado += x.Monto);
            return totalProyectado;
        }

    }
}
