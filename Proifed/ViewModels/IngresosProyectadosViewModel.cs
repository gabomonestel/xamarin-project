﻿using Newtonsoft.Json;
using Proifed.Config;
using Proifed.Data;
using Proifed.Models;
using Proifed.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Proifed.ViewModels
{
    public class IngresosProyectadosViewModel
    {
        private IRepository<IngresoProyectado> IngresoProyectadoRepo;
        private IRepository<Ingreso> IngresosRepo;
        public ObservableCollection<IngresoProyectado> IngresosData { get; private set; } = new ObservableCollection<IngresoProyectado>();
        public PresupuestoViewModel PresupuestoViewModel { get; private set; }
        public double TotalProyectado { get; internal set; }

        public IngresosProyectadosViewModel(ISQLiteConnection connectionService)
        {
            InitIngresoProyectadoRepo(connectionService);
            InitIngresoRepo(connectionService);
            PresupuestoViewModel = new PresupuestoViewModel();
            TotalProyectado = 0;
            //RefreshData();
        }

        public void InitIngresoProyectadoRepo(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                IngresoProyectadoRepo = new Repository<IngresoProyectado>(connectionService);
            }
        }

        public void InitIngresoRepo(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                IngresosRepo = new Repository<Ingreso>(connectionService);
            }
        }

        public async Task<IngresoProyectado> ObtenerIngresoMayorVencimiento()
        {
            var ingresos = IngresoProyectadoRepo.QuerableTable();
            return await ingresos.OrderByDescending(i => i.FechaFinalizacion).FirstAsync();
        }

        public async Task RefreshData()
        {
            TotalProyectado = 0;
            IngresosData.Clear();
            DateTime fechaActual = DateUtil.FechaFinMes();
            List<IngresoProyectado> data = await IngresoProyectadoRepo.QuerableTable()
                                                                      .Where(i => i.FechaCreacion <= fechaActual
                                                                             && i.FechaFinalizacion >= fechaActual)
                                                                      .ToListAsync();
            data.ForEach(x => { IngresosData.Add(x); TotalProyectado += x.Monto; });

        }

        /*public void InitRepo(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                IngresosRepo = new Repository<Ingreso>(connectionService);
            }
        }*/

        public async Task<int> DeleteIngresoProyectado(IngresoProyectado ingreso)
        {
            int borrado = await IngresoProyectadoRepo.Delete(ingreso);
            if (borrado > 0)
            {
                DateTime fechaActual = DateUtil.FechaFinMes();
                int borrados = await IngresosRepo.QuerableTable()
                                                 .Where(i => i.IdProyectado == ingreso.Id
                                                        && i.EsReal == true
                                                        && i.ModificadoUsuario == false
                                                        && i.FechaCreacion >= fechaActual)
                                                 .DeleteAsync();
                return borrado;
            }else{
                return 0;
            }

        }

        public async Task<int> DeleteIngreso(Ingreso ingreso){
            if(ingreso.Id > 0){
                return await IngresosRepo.Delete(ingreso);
            }else{
                return 1;
            }
        }

        /*public int DeleteAll(){
            try
            {
                return IngresosRepo.DeleteAll().Result;
            }catch(Exception ex){
                String message = ex.Message;  
            }
            return -1;
        }*/

    }
}
