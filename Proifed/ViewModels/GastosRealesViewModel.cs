﻿using Newtonsoft.Json;
using Proifed.Config;
using Proifed.Data;
using Proifed.Models;
using Proifed.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Proifed.ViewModels
{
    public class GastosRealesViewModel
    {
        private IRepository<Gasto> GastosRepo;
        private IRepository<GastoProyectado> GastoProyectadoRepo;
        public ObservableCollection<Gasto> GastosData { get; private set; } = new ObservableCollection<Gasto>();
        public PresupuestoViewModel PresupuestoViewModel { get; private set; }
        public double TotalReal { get; set; }
        public DateTime Fecha { get; private set; }

        public GastosRealesViewModel(ISQLiteConnection connectionService, DateTime fecha)
        {
            InitRepos(connectionService);
            PresupuestoViewModel = new PresupuestoViewModel();
            this.Fecha = fecha;
        }

        public void InitRepos(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                GastosRepo = new Repository<Gasto>(connectionService);
                GastoProyectadoRepo = new Repository<GastoProyectado>(connectionService);
            }
        }

        public double TotalGastosProyectados()
        {
            DateTime fechaActual = DateUtil.FechaFinMes();
            List<Gasto> data = GastosRepo.QuerableTable().Where(i => i.EsReal == false
                                                && i.FechaCreacion == fechaActual
                                               ).ToListAsync().Result;
            double totalProyectado = 0;
            data.ForEach(x => totalProyectado += x.Monto);
            return totalProyectado;
        }

        public async Task RefreshData()
        {
            TotalReal = 0;
            GastosData.Clear();
            DateTime fechaActual = DateUtil.FechaFinMes();
            List<Gasto> data = await GastosRepo.QuerableTable()
                                                   .Where(i => i.EsReal == true
                                                          && i.FechaCreacion == fechaActual)
                                                   .ToListAsync();
            data.ForEach(x =>
            {
                GastosData.Add(x);
                TotalReal += x.Monto;
            });

        }

        public int DeleteGasto(Gasto gasto)
        {
            return GastosRepo.Delete(gasto).Result;
        }

        public int DeleteAll()
        {
            try
            {
                return GastosRepo.DeleteAll().Result;
            }
            catch (Exception ex)
            {
                String message = ex.Message;
            }
            return -1;
        }

        /*public async void CrearGastosRealesMes()
        {
            DateTime fechaActual = DateUtil.FechaFinMes();
            if (PresupuestoViewModel.Presupuesto != null) 
            {

                List<GastoProyectado> gastosProyectados = await GastoProyectadoRepo.QuerableTable()
                                                 .Where(i => i.FechaCreacion <= fechaActual
                                                        && i.FechaFinalizacion >= fechaActual
                                                        && i.FechaUltimaGeneracion != fechaActual)
                                                 .ToListAsync();


                gastosProyectados.ForEach(async gasto =>
                {
                    Gasto lGastoProyectado = new Gasto();
                    lGastoProyectado = CopyUtil.CopiarGastoProyectado(gasto, lGastoProyectado, false);
                    await GastosRepo.Insert(lGastoProyectado);

                    Gasto lGastoReal = new Gasto();
                    lGastoReal = CopyUtil.CopiarGastoProyectado(gasto, lGastoReal, true);
                    await GastosRepo.Insert(lGastoReal);

                    gasto.FechaUltimaGeneracion = fechaActual;
                    await GastoProyectadoRepo.Update(gasto);

                });
                await RefreshData();
            }
        }*/

    }
}
