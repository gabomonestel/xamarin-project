﻿using Newtonsoft.Json;
using Proifed.Config;
using Proifed.Data;
using Proifed.Models;
using Proifed.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Proifed.ViewModels
{
    public class AnotacionesViewModel
    {
        IRepository<Anotacion> AnotacionesRepo;
        public ObservableCollection<Anotacion> AnotacionesData { get; private set; } = new ObservableCollection<Anotacion>();
        public PresupuestoViewModel PresupuestoViewModel { get; private set; }
        public DateTime Fecha { get; private set; }

        public AnotacionesViewModel(ISQLiteConnection connectionService, DateTime fecha)
        {
            this.Fecha = fecha;
            InitAnotacionRepo(connectionService);
            PresupuestoViewModel = new PresupuestoViewModel();
        }


        public void InitAnotacionRepo(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                AnotacionesRepo = new Repository<Anotacion>(connectionService);
            }
        }


        public async Task RefreshData()
        {
            DateTime fechaInicio = new DateTime(Fecha.Year, Fecha.Month, 1);
            DateTime fechaFin = fechaInicio.AddMonths(1);
            AnotacionesData.Clear();
            List<Anotacion> data = await AnotacionesRepo.QuerableTable()
                                                        .Where(i => i.Fecha >= fechaInicio
                                                               && i.Fecha < fechaFin)
                                                   .ToListAsync();
            data.ForEach(x =>
            {
                AnotacionesData.Add(x);
            });
        }

        public async Task<int> DeleteAnotacion(Anotacion anotacion)
        {
            return await AnotacionesRepo.Delete(anotacion);
        }

        public async Task<int> GuardarAnotacion(Anotacion anotacion){
            return await AnotacionesRepo.Insert(anotacion);
        }

    }
}
