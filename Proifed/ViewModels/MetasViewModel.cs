﻿using Proifed.Data;
using Proifed.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace Proifed.ViewModels
{
    public class MetasViewModel
    {
        private IRepository<Meta> MetaRepo;
        public ObservableCollection<Meta> MetasData { get; private set; } = new ObservableCollection<Meta>();

        public MetasViewModel(ISQLiteConnection connectionService)
        {
            InitRepo(connectionService);
        }

        public void RefreshData()
        {
            MetasData.Clear();
            List<Meta> data = MetaRepo.GetAllAsync().Result;
            data.ForEach(x => MetasData.Add(x));

            Debug.WriteLine("lista de metas");
        }

        public int DeleteMeta(Meta meta)
        {
            return MetaRepo.Delete(meta).Result;
        }

        public void InitRepo(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                MetaRepo = new Repository<Meta>(connectionService);
            }
        }

    }
}
