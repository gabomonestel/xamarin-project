﻿using Newtonsoft.Json;
using Proifed.Config;
using Proifed.Data;
using Proifed.Models;
using Proifed.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Proifed.ViewModels
{
    public class AgregarGastoProyectadoViewModel
    {
        private IRepository<GastoProyectado> GastoProyectadoRepo;
        private IRepository<Gasto> GastosRepo;
        public PresupuestoViewModel PresupuestoViewModel { get; private set; }
        public GastoProyectado Gasto { get; set; }

        public AgregarGastoProyectadoViewModel(ISQLiteConnection connectionService)
        {
            InitGastoProyectadoRepo(connectionService);
            InitGastoRepo(connectionService);
            PresupuestoViewModel = new PresupuestoViewModel();
        }


        public AgregarGastoProyectadoViewModel(ISQLiteConnection connectionService, GastoProyectado gasto)
        {
            InitGastoProyectadoRepo(connectionService);
            InitGastoRepo(connectionService);
            PresupuestoViewModel = new PresupuestoViewModel();
            this.Gasto = gasto;
        }

        public void InitGastoProyectadoRepo(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                GastoProyectadoRepo = new Repository<GastoProyectado>(connectionService);
            }
        }

        public void InitGastoRepo(ISQLiteConnection connectionService)
        {
            if (connectionService.GetConnection() != null)
            {
                GastosRepo = new Repository<Gasto>(connectionService);
            }
        }

        public async Task<int> GuardarGastoProyectado(GastoProyectado gasto, int plazo)
        {
            bool creando = gasto.Id == 0;

            int guardado = 0;
            DateTime fechaActual = DateUtil.FechaFinMes();
            if (creando)
            {
                gasto.FechaUltimaGeneracion = fechaActual;
                guardado = await GastoProyectadoRepo.Insert(gasto);

                if(guardado != 0) {
                    //Guarda los datos reales futuros
                    for (int i = 0; i < plazo; i++)
                    {
                        Gasto lGastoReal = new Gasto();
                        lGastoReal = CopyUtil.CopiarGastoProyectado(gasto, lGastoReal, true);
                        lGastoReal.FechaCreacion = lGastoReal.FechaCreacion.AddMonths(i);
                        guardado = await GuardarGasto(lGastoReal);
                        Gasto lGastoProyectado = new Gasto();
                        lGastoProyectado = CopyUtil.CopiarGastoProyectado(gasto, lGastoProyectado, false);
                        lGastoProyectado.FechaCreacion = lGastoProyectado.FechaCreacion.AddMonths(i);
                        guardado = await GuardarGasto(lGastoProyectado);
                    }
                }

            }
            else
            {
                await GastoProyectadoRepo.Update(gasto);

                //Borra los gastos reales futuros previamente generados
                int borrados = await GastosRepo.QuerableTable()
                .Where(i => i.IdProyectado == gasto.Id
                       //&& i.EsReal == true
                       && i.ModificadoUsuario == false
                       && i.FechaCreacion >= fechaActual)
                       .DeleteAsync();

                //Guarda los nuevos datos reales futuros
                for (int i = 0; i < plazo; i++)
                {
                    Gasto lGastoReal = new Gasto();
                    lGastoReal = CopyUtil.CopiarGastoProyectado(gasto, lGastoReal, true);
                    lGastoReal.FechaCreacion = lGastoReal.FechaCreacion.AddMonths(i);
                    guardado = await GuardarGasto(lGastoReal);
                    Gasto lGastoProyectado = new Gasto();
                    lGastoProyectado = CopyUtil.CopiarGastoProyectado(gasto, lGastoProyectado, false);
                    lGastoProyectado.FechaCreacion = lGastoProyectado.FechaCreacion.AddMonths(i);
                    guardado = await GuardarGasto(lGastoProyectado);
                }
            }

            return guardado;
        }


        public async Task<int> GuardarGasto(Gasto gasto)
        {
            if (gasto.Id > 0)
            {
                return await GastosRepo.Update(gasto);
            }
            else
            {
                return await GastosRepo.Insert(gasto);
            }
        }

    }
}
