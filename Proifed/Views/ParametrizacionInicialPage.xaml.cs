﻿using System;
using Xamarin.Forms;
using Proifed.ViewModels;
using Proifed.Config;
using Xamarin.Essentials;
using Proifed.Models;

namespace Proifed.Views
{
    public partial class ParametrizacionInicialPage : ContentPage
    {

        readonly ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public ParametrizacionInicialPage()
        {
            //IngresosRealesViewModel ingresosViewModel = new IngresosRealesViewModel();
            //ingresosViewModel.InitRepo(ConnectionService);
            //ingresosViewModel.DeleteAll();

            BindingContext = new ParametrizacionInicialViewModel(ConnectionService);
            InitializeComponent();
            initPickers();
        }

        void initPickers()
        {
            pickerMes.ItemsSource = DataRepositoryUtil.obtMeses();
            pickerAnno.ItemsSource = DataRepositoryUtil.obtAnnos();

            pickerMes.SelectedIndex = DateTime.Now.Month - 1;
            pickerAnno.SelectedIndex = 0;
        }

        async void Next_Clicked(object sender, System.EventArgs e)
        {
            Presupuesto presupuesto = new Presupuesto();
            presupuesto.Configurando = true;
            presupuesto.PasoActual = Constants.PASO_INGRESOS;
            presupuesto.FechaInicio = new DateTime((int)pickerAnno.SelectedItem, ((Mes)pickerMes.SelectedItem).Id, 1);

            var viewModel = BindingContext as ParametrizacionInicialViewModel;

            viewModel.PresupuestoViewModel.GuardarPresupuesto(presupuesto);
            await Navigation.PushAsync(new IngresosProyectadosPage());
            Navigation.RemovePage(this);
        }

    }
}
