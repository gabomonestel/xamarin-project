﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Proifed.Views;
using Proifed.ViewModels;
using Proifed.Models;

namespace Proifed.Views
{
    public partial class MetasPage : ContentPage
    {
        private readonly ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public MetasPage()
        {
            BindingContext = new MetasViewModel(ConnectionService);
            InitializeComponent();

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            Refrescar();
        }


        void Refrescar(){
            MetasViewModel viewModel = ((MetasViewModel)BindingContext);
            viewModel.RefreshData();
            lstMetas.IsVisible = viewModel.MetasData.Count > 0;
            stlEmpty.IsVisible = viewModel.MetasData.Count == 0;
        }

        void AgregarMeta_Clicked(object sender, System.EventArgs e)
        {
            if ((BindingContext as MetasViewModel).MetasData.Count < 5)
            {
                Navigation.PushAsync(new AgregarMetaPage());
            }
            else{
                DisplayAlert("Problema", "Solo se permite agregar máximo 5 metas", "Ok");
            }
        }

        void BorrarMeta_Clicked(object sender, System.EventArgs e)
        {
            Meta meta = (Meta)(((MenuItem)sender).CommandParameter);
            int deleted = ((MetasViewModel)BindingContext).DeleteMeta(meta);
            if (deleted > 0)
            {
                Refrescar();
            }
        }
    }
}
