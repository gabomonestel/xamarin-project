﻿using System;
using Proifed.ViewModels;
using Xamarin.Forms;

namespace Proifed.Views
{
    public partial class ResumenAhorrosPage : ContentPage
    {
        private readonly ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public ResumenAhorrosPage(DateTime fechaInicio, DateTime fechaFin)
        {
            InitializeComponent();
            BindingContext = new ResumenAhorrosViewModel(ConnectionService, fechaInicio, fechaFin);
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
            Refrescar();
        }

        async void Refrescar()
        {
            var viewModel = BindingContext as ResumenAhorrosViewModel;
            await viewModel.CargarDatos();
            lstAhorros.IsVisible = viewModel.Ahorros.Count > 0;
            stlEmpty.IsVisible = viewModel.Ahorros.Count == 0;
        }

        void IrAhorros_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new AhorrosPage());
        }
    }
}
