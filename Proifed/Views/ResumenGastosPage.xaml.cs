﻿using System;
using Proifed.Config;
using Proifed.ViewModels;
using Xamarin.Forms;

namespace Proifed.Views
{
    public partial class ResumenGastosPage : ContentPage
    {
        private readonly ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public ResumenGastosPage(int tipo, DateTime fechaInicio, DateTime fechaFin)
        {
            InitializeComponent();
            BindingContext = new ResumenGastosViewModel(ConnectionService, tipo, fechaInicio, fechaFin);
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
            Refrescar();
        }

        async void Refrescar()
        {
            var viewModel = BindingContext as ResumenGastosViewModel;
            await viewModel.CargarDatos();
            lstGastos.IsVisible = viewModel.Gastos.Count > 0;
            stlEmpty.IsVisible = viewModel.Gastos.Count == 0;
        }

        void IrGastos_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new PresupuestoPage(Constants.TAB_GASTOS));
        }

    }
}
