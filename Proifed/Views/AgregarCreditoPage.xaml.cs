﻿using System;
using System.Collections.Generic;
using Proifed.Config;
using Proifed.Models;
using Proifed.ViewModels;
using Xamarin.Forms;

namespace Proifed.Views
{
    public partial class AgregarCreditoPage : ContentPage
    {
        private readonly ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public AgregarCreditoPage()
        {
            BindingContext = new AgregarCreditoViewModel(ConnectionService);
            InitializeComponent();
            InitPickers();
        }

        private void InitPickers()
        {
            pickerTipo.ItemsSource = (BindingContext as AgregarCreditoViewModel).Prestamos;
        }

        void Tipo_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            lblDescripcion.IsVisible = false;
            txtDescripcion.IsVisible = false;
            lblRequireDescripcion.IsVisible = false;
            TipoPrestamo tipo = (sender as Picker).SelectedItem as TipoPrestamo;
            if(tipo != null && tipo.Descripcion.Equals("Otros")){
                lblDescripcion.IsVisible = true;
                txtDescripcion.IsVisible = true;
                lblRequireDescripcion.IsVisible = true;
            }
        }

        async void AgregarPresupuesto_Clicked(object sender, System.EventArgs e)
        {
            lblRequireTipo.TextColor = Color.Black;
            lblRequireMonto.TextColor = Color.Black;
            lblRequireCuota.TextColor = Color.Black;
            lblRequireInteres.TextColor = Color.Black;
            lblRequirePlazo.TextColor = Color.Black;

            double monto = String.IsNullOrEmpty(txtMonto.Text) ? -1 : Double.Parse(txtMonto.Text);
            double cuota = String.IsNullOrEmpty(txtCuota.Text) ? -1 : Double.Parse(txtCuota.Text);
            double interes = String.IsNullOrEmpty(txtInteres.Text) ? -1 : Double.Parse(txtInteres.Text);
            int plazo = String.IsNullOrEmpty(txtPlazo.Text) ? -1 : Int32.Parse(txtPlazo.Text);
            TipoPrestamo tipo = pickerTipo.SelectedItem as TipoPrestamo;

            string descripcion = null;
            if (tipo != null)
            {
                if (!tipo.Descripcion.Equals("Otros"))
                {
                    txtDescripcion.Text = tipo.Descripcion;
                    descripcion = tipo.Descripcion;
                }
                else
                {
                    descripcion = txtDescripcion.Text;
                }
            }

            bool tieneErrores = false;
            if(monto < 0){
                tieneErrores = true;
                lblRequireMonto.TextColor = Color.Red;
                lblRequireMonto.FontAttributes = FontAttributes.Bold;
            }
            if(cuota < 0){
                tieneErrores = true;
                lblRequireCuota.TextColor = Color.Red;
                lblRequireCuota.FontAttributes = FontAttributes.Bold;
            }
            if(tipo == null){
                tieneErrores = true;
                lblRequireTipo.TextColor = Color.Red;
                lblRequireTipo.FontAttributes = FontAttributes.Bold;
            }
            if(String.IsNullOrEmpty(descripcion)){
                tieneErrores = true;
                lblRequireDescripcion.TextColor = Color.Red;
                lblRequireDescripcion.FontAttributes = FontAttributes.Bold;
            }
            if(plazo < 0){
                tieneErrores = true;
                lblRequirePlazo.TextColor = Color.Red;
                lblRequirePlazo.FontAttributes = FontAttributes.Bold;
            }
            if(interes < 0){
                tieneErrores = true;
                lblRequireInteres.TextColor = Color.Red;
                lblRequireInteres.FontAttributes = FontAttributes.Bold;
            }
            

            if(tieneErrores)
            {
                return;
            }

            Credito credito;

            AgregarCreditoViewModel viewModel = BindingContext as AgregarCreditoViewModel;
            if (viewModel.Credito != null)
            {
                credito = viewModel.Credito;
            }
            else
            {
                credito = new Credito();
            }

            DateTime fechaActual = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            fechaActual = fechaActual.AddMonths(1);
            fechaActual = fechaActual.AddDays(-1);
            DateTime fechaFinalizacion = fechaActual.AddMonths(plazo);

            credito.Tipo = tipo.Id;
            credito.Descripcion = descripcion;
            credito.Monto = monto;
            credito.CuotaMensual = cuota;
            credito.Plazo = plazo;
            credito.FechaInicio = fechaActual;
            credito.FechaFinalizacion = fechaFinalizacion;

            int idCredito = await viewModel.GuardarCredito(credito);
            if (idCredito > 0)
            {
                await Navigation.PopAsync();
            }
            else
            {
                await DisplayAlert("Error", "Algo salió mal", "Cerrar");
            }


            /*CreditoViewModel viewModel = (BindingContext as CreditoViewModel);
            if (viewModel.Presupuesto != null)
            {
                //Do something
            }
            else
            {
                var result = await DisplayAlert("Presupuesto no configurado", "Desea configurar el presupuesto?", "Crear", "Cancelar");
            }*/

            //double interes = prestamo.InteresAnual / 100;
            //double interesMensual = interes / 12;
            //double cuota = monto / ((1 - (1 / Math.Pow(1 + interesMensual, plazo.PlazoMeses))) / interesMensual);
            //lblCuotaMensual.Text = String.Format("{0:N}", cuota);
            //guardarButton.IsVisible = true;

        }

        void CalcularCuota_Clicked(object sender, System.EventArgs e)
        {

        }

    }
}
