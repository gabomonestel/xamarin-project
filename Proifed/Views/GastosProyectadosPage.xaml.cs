﻿using System;
using System.Threading.Tasks;
using Proifed.Config;
using Proifed.Models;
using Proifed.ViewModels;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Proifed.Views
{
    public partial class GastosProyectadosPage : ContentPage
    {
        private readonly ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public GastosProyectadosPage()
        {
            BindingContext = new GastosProyectadosViewModel(ConnectionService);
            InitializeComponent();
            //Preferences.Set(Constants.MOSTRAR_AYUDA_GASTOS_PROYECTADOS, true);
        }


        private async void Refrescar()
        {
            if (Preferences.Get(Constants.MOSTRAR_AYUDA_GASTOS_PROYECTADOS, true))
            {
                stlFirstTime.IsVisible = true;
                stlEmpty.IsVisible = false;
                lstGastos.IsVisible = false;
            }
            else
            {

                GastosProyectadosViewModel viewModel = (GastosProyectadosViewModel)BindingContext;
                await viewModel.RefreshData();
                stlFirstTime.IsVisible = false;
                lstGastos.IsVisible = viewModel.GastosData.Count > 0;
                stlEmpty.IsVisible = viewModel.GastosData.Count == 0;
                lblTotalGastos.Text = String.Format("{0:N}", viewModel.TotalProyectado);
                loadTitle();
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            Refrescar();
        }

        void loadTitle()
        {
            GastosProyectadosViewModel viewModel = (GastosProyectadosViewModel)BindingContext;
            if (viewModel.PresupuestoViewModel.Presupuesto != null && viewModel.PresupuestoViewModel.Presupuesto.Configurando)
            {
                Title = "Configuración Inicial";
                stlConfigurando.IsVisible = true;
                btnSiguiente.IsVisible = true;
            }
        }


        void Entendido_Clicked(object sender, EventArgs e)
        {
            Preferences.Set(Constants.MOSTRAR_AYUDA_GASTOS_PROYECTADOS, false);
            Refrescar();
        }

        void Nuevo_Clicked(object sender, System.EventArgs e)
        {
            if (Preferences.Get(Constants.MOSTRAR_AYUDA_GASTOS_PROYECTADOS, true))
            {
                Preferences.Set(Constants.MOSTRAR_AYUDA_GASTOS_PROYECTADOS, false);
                Refrescar();
            }
            Navigation.PushAsync(new AgregarGastoProyectadoPage());
        }


        private async void DeleteGasto_Clicked(object sender, System.EventArgs e)
        {
            GastoProyectado gasto = (GastoProyectado)(((MenuItem)sender).CommandParameter);
            var viewModel = BindingContext as GastosProyectadosViewModel;
            int deleted = await viewModel.DeleteGastoProyectado(gasto);
            Refrescar();
        }

        async void Finish_Clicked(object sender, System.EventArgs e)
        {
            if (Preferences.Get(Constants.MOSTRAR_AYUDA_GASTOS_PROYECTADOS, true))
            {
                Preferences.Set(Constants.MOSTRAR_AYUDA_GASTOS_PROYECTADOS, false);
            }
            var viewModel = (BindingContext as GastosProyectadosViewModel);

            Presupuesto presupuesto = viewModel.PresupuestoViewModel.Presupuesto;
            presupuesto.Configurando = false;

            viewModel.PresupuestoViewModel.GuardarPresupuesto(presupuesto);

            await Navigation.PushAsync(new HomePage());
            Navigation.RemovePage(this);
        }


        void Gasto_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (Preferences.Get(Constants.MOSTRAR_AYUDA_GASTOS_PROYECTADOS, true))
            {
                Preferences.Set(Constants.MOSTRAR_AYUDA_GASTOS_PROYECTADOS, false);
                Refrescar();
            }
            Navigation.PushAsync(new AgregarGastoProyectadoPage(e.Item as GastoProyectado));
        }
    }
}