﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Proifed.Config;
using Proifed.Models;
using Proifed.Util;
using Proifed.ViewModels;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Proifed.Views
{
    public partial class SplashPage : ContentPage
    {

        ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public SplashPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            BindingContext = new PresupuestoViewModel();
            InitializeComponent();
        }

        protected override async void OnAppearing(){
            base.OnAppearing();
            await CheckPresupuesto();
        }

        private async Task CheckPresupuesto()
        {
            var viewModel = BindingContext as PresupuestoViewModel;
            Presupuesto presupuesto = viewModel.Presupuesto;
            if (presupuesto == null)
            {
                Application.Current.MainPage = new NavigationPage(new ParametrizacionInicialPage())
                {
                    BarBackgroundColor = Color.FromHex("#fd8000"),
                    BarTextColor = Color.White
                };
            }
            else
            {
                if (presupuesto.Configurando == true)
                {
                    if (presupuesto.PasoActual != Constants.PASO_PRESUPUESTO)
                    {
                        /*bool continuar = await DisplayAlert("Configuración en proceso",
                                     "Actualmente existe una configuración sin finalizar, desea continuar configurando",
                                     "Continuar", "Nuevo");*/

                        if (presupuesto.FechaInicio == DateUtil.FechaInicioMes())//continuar)
                        {
                            if (presupuesto.PasoActual == Constants.PASO_INGRESOS)
                            {
                                Application.Current.MainPage = new NavigationPage(new IngresosProyectadosPage())
                                {
                                    BarBackgroundColor = Color.FromHex("#fd8000"),
                                    BarTextColor = Color.White
                                };
                            }
                            else if (presupuesto.PasoActual == Constants.PASO_GASTOS)
                            {
                                Application.Current.MainPage = new NavigationPage(new GastosProyectadosPage())
                                {
                                    BarBackgroundColor = Color.FromHex("#fd8000"),
                                    BarTextColor = Color.White
                                };
                            }
                        }
                        else
                        {
                            Preferences.Set(Constants.PRESUPUESTO, "");
                            Application.Current.MainPage = new NavigationPage(new ParametrizacionInicialPage())
                            {
                                BarBackgroundColor = Color.FromHex("#fd8000"),
                                BarTextColor = Color.White
                            };
                        }
                    }
                    else
                    {
                        Preferences.Set(Constants.PRESUPUESTO, "");
                        Application.Current.MainPage = new NavigationPage(new ParametrizacionInicialPage())
                        {
                            BarBackgroundColor = Color.FromHex("#fd8000"),
                            BarTextColor = Color.White
                        };
                    }
                }
                else
                {
                    //var ingresosViewModel = new IngresosRealesViewModel(DependencyService.Get<ISQLiteConnection>(), DateUtil.FechaFinMes());
                    //var gastosViewModel = new GastosRealesViewModel(DependencyService.Get<ISQLiteConnection>(), DateUtil.FechaFinMes());

                    //await ingresosViewModel.CrearIngresosRealesMes();

                    //await gastosViewModel.CrearGastosRealesMes();

                    Application.Current.MainPage = new NavigationPage(new HomePage())
                    {
                        BarBackgroundColor = Color.FromHex("#fd8000"),
                        BarTextColor = Color.White
                    };
                }
            }
        }

    }
}
