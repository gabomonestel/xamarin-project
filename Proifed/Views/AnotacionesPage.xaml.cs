﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Proifed.ViewModels;
using Proifed.Config;
using Proifed.Models;
using Xamarin.Essentials;

namespace Proifed.Views
{
    public partial class AnotacionesPage : ContentPage
    {

        private readonly ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public AnotacionesPage(DateTime fecha)
        {
            BindingContext = new AnotacionesViewModel(ConnectionService, fecha);
            InitializeComponent();
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
            Refrescar();
        }

        private async void Refrescar(){
            AnotacionesViewModel viewModel = BindingContext as AnotacionesViewModel;
            await viewModel.RefreshData();
            lstAnotaciones.IsVisible = viewModel.AnotacionesData.Count > 0;
            stlEmpty.IsVisible = viewModel.AnotacionesData.Count == 0;
        }

        async void Guardar_Clicked(object sender, EventArgs e)
        {
            var viewModel = BindingContext as AnotacionesViewModel;

            string descripcionAnotacion = txtAnotacion.Text;
            if(string.IsNullOrEmpty(descripcionAnotacion) || string.IsNullOrWhiteSpace(descripcionAnotacion)){
                return;
            }
            Anotacion anotacion = new Anotacion
            {
                Descripcion = descripcionAnotacion,
                Fecha = new DateTime(viewModel.Fecha.Year, viewModel.Fecha.Month, DateTime.Now.Day)
            };
            int guardado = await viewModel.GuardarAnotacion(anotacion);
            if (guardado > 0)
            {
                txtAnotacion.Text = "";
                Refrescar();
            }
        }

        async void DeleteAnotacion_Clicked(object sender, EventArgs e)
        {
            Anotacion anotacion = (Anotacion)(((MenuItem)sender).CommandParameter);
            int deleted = await (BindingContext as AnotacionesViewModel).DeleteAnotacion(anotacion);
            if (deleted > 0)
            {
                Refrescar();
            }
        }

    }
}
