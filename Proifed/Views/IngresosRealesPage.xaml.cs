﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Proifed.ViewModels;
using Proifed.Config;
using Proifed.Models;
using Xamarin.Essentials;
using Proifed.Util;

namespace Proifed.Views
{
    public partial class IngresosRealesPage : ContentPage
    {

        private readonly ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public IngresosRealesPage(DateTime fecha)
        {
            BindingContext = new IngresosRealesViewModel(ConnectionService, fecha);
            InitializeComponent();
            //Preferences.Set(Constants.MOSTRAR_AYUDA_INGRESOS_REALES, true);
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
            Refrescar();
        }

        private async void Refrescar(){
            if (Preferences.Get(Constants.MOSTRAR_AYUDA_INGRESOS_REALES, true))
            {
                stlFirstTime.IsVisible = true;
                stlEmpty.IsVisible = false;
                lstIngresos.IsVisible = false;
            }
            else { 
                IngresosRealesViewModel viewModel = BindingContext as IngresosRealesViewModel;
                await viewModel.RefreshData();
                stlFirstTime.IsVisible = false;
                lstIngresos.IsVisible = viewModel.IngresosData.Count > 0;
                stlEmpty.IsVisible = viewModel.IngresosData.Count == 0;
                lblTotalIngresos.Text = String.Format("{0:N}", (BindingContext as IngresosRealesViewModel).TotalIngresosProyectados());
                lblTotalReal.Text = String.Format("{0:N}", (BindingContext as IngresosRealesViewModel).TotalReal);
            }
        }

        void Entendido_Clicked(object sender, EventArgs e)
        {
            Preferences.Set(Constants.MOSTRAR_AYUDA_INGRESOS_REALES, false);
            Refrescar();
        }

        void Nuevo_Clicked(object sender, EventArgs e)
        {
            if (Preferences.Get(Constants.MOSTRAR_AYUDA_INGRESOS_REALES, true))
            {
                Preferences.Set(Constants.MOSTRAR_AYUDA_INGRESOS_REALES, false);
                Refrescar();
            }
            Navigation.PushAsync(new AgregarIngresoPage());
        }

        void Anotaciones_Clicked(object sender, EventArgs e)
        {
            if (Preferences.Get(Constants.MOSTRAR_AYUDA_INGRESOS_REALES, true))
            {
                Preferences.Set(Constants.MOSTRAR_AYUDA_INGRESOS_REALES, false);
                Refrescar();
            }
            Navigation.PushAsync(new AnotacionesPage(DateUtil.FechaFinMes()));
        }

        void DeleteIngreso_Clicked(object sender, EventArgs e)
        {
            Ingreso ingreso = (Ingreso)(((MenuItem)sender).CommandParameter);
            int deleted = (BindingContext as IngresosRealesViewModel).DeleteIngreso(ingreso);
            if (deleted > 0)
            {
                Refrescar();
            }
        }

        void VerProyectados_Clicked(object sender, EventArgs e)
        {
            if (Preferences.Get(Constants.MOSTRAR_AYUDA_INGRESOS_REALES, true))
            {
                Preferences.Set(Constants.MOSTRAR_AYUDA_INGRESOS_REALES, false);
                Refrescar();
            }
            Navigation.PushAsync(new IngresosProyectadosPage());
        }


        void Ingreso_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            if (Preferences.Get(Constants.MOSTRAR_AYUDA_INGRESOS_REALES, true))
            {
                Preferences.Set(Constants.MOSTRAR_AYUDA_INGRESOS_REALES, false);
                Refrescar();
            }
            Navigation.PushAsync(new AgregarIngresoPage(e.Item as Ingreso));
        }


        /*private void CargarListaAnnos()
        {
            for (int i = 2018; i < 2020; i++)
            {
                Button boton = new Button { Text = "" + i, 
                    TextColor = DateTime.Now.Year == i ? Color.White : Color.FromRgb(232, 125, 30), 
                    BackgroundColor = DateTime.Now.Year == i ? Color.FromRgb(232, 125, 30) : Color.Transparent,
                    HeightRequest =24,
                    Padding = 4
                };
                stlAnnos.Children.Add(boton);
            }
            DateTime fechaActual = DateTime.Now;
            double totalTranslation = 0;
            for (int i = 1; i <= 12; i++)
            {
                DateTime date = new DateTime(DateTime.Now.Year, i, 1);
                Button boton = new Button { Text = DataRepositoryUtil.obtMeses()[date.Month - 1].Nombre,
                    TextColor = fechaActual.Month == i ? Color.White : Color.FromRgb(232, 125, 30),
                    BackgroundColor = fechaActual.Month == i ? Color.FromRgb(232, 125, 30) : Color.Transparent,
                    HeightRequest =24,
                    Padding = 4 };
                stlMeses.Children.Add(boton);
                if (fechaActual.Month <= i)
                {
                    totalTranslation += 20; //boton.WidthRequest;
                }
            }
            stlMeses.TranslationX = -totalTranslation;
        }*/

    }
}
