﻿using System;

using Xamarin.Forms;
using Proifed.ViewModels;
using Proifed.Config;
using Proifed.Models;

namespace Proifed.Views
{
    public partial class ResumenIngresosPage : ContentPage
    {
        private readonly ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public ResumenIngresosPage(int tipo, DateTime fechaInicio, DateTime fechaFin)
        {
            InitializeComponent();
            BindingContext = new ResumenIngresosViewModel(ConnectionService, tipo, fechaInicio, fechaFin);
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
            Refrescar();
        }

        async void Refrescar()
        {
            var viewModel = BindingContext as ResumenIngresosViewModel;
            await viewModel.CargarDatos();
            lstIngresos.IsVisible = viewModel.Ingresos.Count > 0;
            stlEmpty.IsVisible = viewModel.Ingresos.Count == 0;
        }

        void IrIngresos_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new PresupuestoPage(Constants.TAB_AHORROS));
        }
    }
}
