﻿using System;

using Xamarin.Forms;
using Proifed.ViewModels;
using Proifed.Config;
using Proifed.Models;
using Xamarin.Essentials;

namespace Proifed.Views
{
    public partial class IngresosProyectadosPage : ContentPage
    {
        private readonly ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public IngresosProyectadosPage()
        {
            InitializeComponent();
            BindingContext = new IngresosProyectadosViewModel(ConnectionService);
            //Preferences.Set(Constants.MOSTRAR_AYUDA_INGRESOS_PROYECTADOS, true);
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
            Refrescar();
        }

        private async void Refrescar(){
            if (Preferences.Get(Constants.MOSTRAR_AYUDA_INGRESOS_PROYECTADOS, true))
            {
                stlFirstTime.IsVisible = true;
                stlEmpty.IsVisible = false;
                lstIngresos.IsVisible = false;
            }
            else
            {
                IngresosProyectadosViewModel viewModel = (IngresosProyectadosViewModel)BindingContext;
                await viewModel.RefreshData();
                stlFirstTime.IsVisible = false;
                lstIngresos.IsVisible = viewModel.IngresosData.Count > 0;
                stlEmpty.IsVisible = viewModel.IngresosData.Count == 0;
                lblTotalIngresos.Text = String.Format("{0:N}", viewModel.TotalProyectado);
                loadTitle();
            }
        }

        void loadTitle()
        {
            IngresosProyectadosViewModel viewModel = (IngresosProyectadosViewModel)BindingContext;
            if (viewModel.PresupuestoViewModel.Presupuesto != null && viewModel.PresupuestoViewModel.Presupuesto.Configurando){
                Title = "Configuración Inicial";
                stlConfigurando.IsVisible = true;
                btnSiguiente.IsVisible = true;
            }
        }

        void Entendido_Clicked(object sender, EventArgs e)
        {
            Preferences.Set(Constants.MOSTRAR_AYUDA_INGRESOS_PROYECTADOS, false);
            Refrescar();
        }

        void Nuevo_Clicked(object sender, System.EventArgs e)
        {
            if (Preferences.Get(Constants.MOSTRAR_AYUDA_INGRESOS_PROYECTADOS, true))
            {
                Preferences.Set(Constants.MOSTRAR_AYUDA_INGRESOS_PROYECTADOS, false);
                Refrescar();
            }
            Navigation.PushAsync(new AgregarIngresoProyectadoPage());
        }

        async void Siguiente_Clicked(object sender, System.EventArgs e)
        {
            if (Preferences.Get(Constants.MOSTRAR_AYUDA_INGRESOS_PROYECTADOS, true))
            {
                Preferences.Set(Constants.MOSTRAR_AYUDA_INGRESOS_PROYECTADOS, false);
                Refrescar();
            }
            IngresosProyectadosViewModel viewModel = (IngresosProyectadosViewModel)BindingContext;
            if (viewModel.IngresosData.Count > 0)
            {
                IngresoProyectado ingresoMayorVencimiento = await viewModel.ObtenerIngresoMayorVencimiento();
                if (ingresoMayorVencimiento != null)
                {
                    var result = await DisplayAlert("Presupuesto configurado correctamente!",
                                                    "Gracias, su aplicación le permitirá programar su presupuesto hasta el mes de "
                                                    + DataRepositoryUtil.obtMeses()[ingresoMayorVencimiento.FechaFinalizacion.Month - 1].Nombre + " de "
                                                    + ingresoMayorVencimiento.FechaFinalizacion.Year
                                                    + ", Desea agregar gastos proyectados?",
                                                    "Ir a gastos", "Finalizar");

                    if(result)
                    {
                        Presupuesto presupuesto = viewModel.PresupuestoViewModel.Presupuesto;
                        presupuesto.PasoActual = Constants.PASO_GASTOS;
                        viewModel.PresupuestoViewModel.GuardarPresupuesto(presupuesto);
                        await Navigation.PushAsync(new GastosProyectadosPage());
                    }
                    else
                    {
                        Presupuesto presupuesto = viewModel.PresupuestoViewModel.Presupuesto;
                        presupuesto.Configurando = false;
                        viewModel.PresupuestoViewModel.GuardarPresupuesto(presupuesto);
                        await Navigation.PushAsync(new HomePage());//PresupuestoPage());
                    }
                    Navigation.RemovePage(this);
                }
            }else{
                await DisplayAlert("Ingresos requeridos", "Debe ingresar al menos 1 ingreso para continuar", "Aceptar");
            }
        }

        private async void DeleteIngreso_Clicked(object sender, EventArgs e)
        {
            IngresoProyectado ingreso = (IngresoProyectado)(((MenuItem)sender).CommandParameter);
            var viewModel = (BindingContext as IngresosProyectadosViewModel);
            int deleted = await viewModel.DeleteIngresoProyectado(ingreso);
            Refrescar();
        }


        void Ingreso_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (Preferences.Get(Constants.MOSTRAR_AYUDA_INGRESOS_PROYECTADOS, true))
            {
                Preferences.Set(Constants.MOSTRAR_AYUDA_INGRESOS_PROYECTADOS, false);
                Refrescar();
            }
            Navigation.PushAsync(new AgregarIngresoProyectadoPage(e.Item as IngresoProyectado));
        }


    }
}
