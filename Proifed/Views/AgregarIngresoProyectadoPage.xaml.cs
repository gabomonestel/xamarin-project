﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Proifed.Config;
using Proifed.Models;
using Proifed.Util;
using Proifed.ViewModels;
using Xamarin.Forms;

namespace Proifed.Views
{
    public partial class AgregarIngresoProyectadoPage : ContentPage
    {
        private readonly ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public AgregarIngresoProyectadoPage()
        {
            BindingContext = new AgregarIngresoProyectadoViewModel(ConnectionService);
            InitializeComponent();
            InitPickers();
        }

        public AgregarIngresoProyectadoPage(IngresoProyectado ingreso)
        {
            BindingContext = new AgregarIngresoProyectadoViewModel(ConnectionService, ingreso);
            InitializeComponent();
            InitPickers();
            CargarIngreso();
        }

        private void CargarIngreso(){
            AgregarIngresoProyectadoViewModel viewModel = (BindingContext as AgregarIngresoProyectadoViewModel);
            foreach (RubroIngreso rubro in pickerTipo.ItemsSource){
                if (rubro.Id == viewModel.Ingreso.Tipo){
                    pickerTipo.SelectedItem = rubro;
                    break;
                }
            }
            DateTime fechaActual = DateUtil.FechaFinMes();
            DateTime fechaFinalizacion = viewModel.Ingreso.FechaFinalizacion;
            int diferenciaMeses = DataRepositoryUtil.DiferenciaMeses(fechaActual, fechaFinalizacion);
            switch(diferenciaMeses){
                case 0:
                    pickerPlazo.SelectedIndex = 0; break;
                case 2:
                    pickerPlazo.SelectedIndex = 1; break;
                case 5:
                    pickerPlazo.SelectedIndex = 2; break;
                case 11:
                    pickerPlazo.SelectedIndex = 3; break;
                default:
                    pickerPlazo.SelectedIndex = 0; break;
            }
            txtNombre.Text = viewModel.Ingreso.Nombre;
            txtMonto.Text = viewModel.Ingreso.Monto.ToString();

        }



        void InitPickers()
        {
            pickerTipo.ItemsSource = DataRepositoryUtil.obtTiposIngreso();
            pickerPlazo.ItemsSource = DataRepositoryUtil.obtPlazos();
            ResetFormCreate();

        }

        void Cancel_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PopAsync();
        }

        private async void Guardar_Clicked(object sender, System.EventArgs e)
        {
            AgregarIngresoProyectadoViewModel viewModel = BindingContext as AgregarIngresoProyectadoViewModel;

            RubroIngreso rubro = pickerTipo.SelectedItem as RubroIngreso;
            int plazo = pickerPlazo.SelectedItem != null ? (pickerPlazo.SelectedItem as Plazo).Cantidad : -1;
            int tipo = rubro != null ? rubro.Id : -1;
            double monto = txtMonto.Text != null && txtMonto.Text != "" ? double.Parse(txtMonto.Text) : -1;
            string nombre = txtNombre.Text != null && txtNombre.Text != "" ? txtNombre.Text : "";


            bool tieneErrores = false;

            if (plazo == -1)
            {
                lblRequirePlazo.TextColor = Color.Red;
                tieneErrores = true;
            }
            if (tipo == -1)
            {
                lblRequireTipo.TextColor = Color.Red;
                tieneErrores = true;
            }
            if (monto < 0)
            {
                lblRequireMonto.TextColor = Color.Red;
                tieneErrores = true;
            }
            if (nombre.Equals(""))
            {
                lblRequireNombre.TextColor = Color.Red;
                tieneErrores = true;
            }

            if (tieneErrores)
            {
                await MostrarGuardadoCamposRequeridos();
                return;
            }

            IngresoProyectado ingreso;
            if(viewModel.Ingreso != null){
                ingreso = viewModel.Ingreso;
                ingreso.Id = viewModel.Ingreso.Id;
            }else{
                ingreso = new IngresoProyectado();
            }

            ingreso.Tipo = tipo;
            ingreso.Monto = monto;
            ingreso.Nombre = nombre;
            if (plazo != -1)
            {
                DateTime finaliza = DateUtil.FechaFinMes().AddMonths(plazo - 1);
                ingreso.FechaCreacion = DateUtil.FechaFinMes();
                ingreso.FechaFinalizacion = finaliza;
            }

            int idIngreso = await viewModel.GuardarIngresoProyectado(ingreso, plazo);
            if (idIngreso > 0)
            {
                MostrarGuardadoExitoso();
            }
            else
            {
                //Error
            }
        }



        void Tipo_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            RubroIngreso rubro = ((Picker)sender).SelectedItem as RubroIngreso;
            if (rubro != null)
            {
                if (rubro.Nombre.Equals("Otros"))
                {
                    txtNombre.Text = "";
                    lblNombre.IsVisible = true;
                    txtNombre.IsVisible = true;
                    lblRequireNombre.IsVisible = true;
                }
                else
                {
                    lblNombre.IsVisible = false;
                    txtNombre.IsVisible = false;
                    txtNombre.Text = rubro.Nombre;
                    lblRequireNombre.IsVisible = false;
                }
            }
        }

        void ResetFormCreate()
        {
            txtNombre.IsVisible = false;
            lblNombre.IsVisible = false;
            txtMonto.Text = "";
            txtNombre.Text = "";
            pickerTipo.SelectedIndex = -1;
        }

        private void MostrarGuardadoExitoso()
        {
            /*resetFormCreate();
            stlCamposRequeridos.IsVisible = false;
            stlGuardadoExitoso.IsVisible = true;
            await Task.Delay(3000);
            stlGuardadoExitoso.IsVisible = false;*/
            Navigation.PopAsync();
        }

        private async Task MostrarGuardadoCamposRequeridos()
        {
            stlGuardadoExitoso.IsVisible = false;
            stlCamposRequeridos.IsVisible = true;
            await Task.Delay(3000);
            stlCamposRequeridos.IsVisible = false;

        }
    }
}
