﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Proifed.ViewModels;
using Proifed.Config;
using Proifed.Models;
using Xamarin.Essentials;
using Proifed.Util;

namespace Proifed.Views
{
    public partial class GastosRealesPage : ContentPage
    {
        private readonly ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public GastosRealesPage(DateTime fecha)
        {
            BindingContext = new GastosRealesViewModel(ConnectionService, fecha);
            InitializeComponent();
            //Preferences.Set(Constants.MOSTRAR_AYUDA_GASTOS_REALES, true);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            Refrescar();
        }

        async void Refrescar()
        {
            if (Preferences.Get(Constants.MOSTRAR_AYUDA_GASTOS_REALES, true))
            {
                stlFirstTime.IsVisible = true;
                stlEmpty.IsVisible = false;
                lstGastos.IsVisible = false;
            }
            else
            {
                GastosRealesViewModel viewModel = BindingContext as GastosRealesViewModel;
                await viewModel.RefreshData();
                stlFirstTime.IsVisible = false;
                lstGastos.IsVisible = viewModel.GastosData.Count > 0;
                stlEmpty.IsVisible = viewModel.GastosData.Count == 0;
                lblTotalGastos.Text = String.Format("{0:N}", viewModel.TotalGastosProyectados());
                lblTotalReal.Text = String.Format("{0:N}", viewModel.TotalReal);
            }
        }

        void Entendido_Clicked(object sender, EventArgs e)
        {
            Preferences.Set(Constants.MOSTRAR_AYUDA_GASTOS_REALES, false);
            Refrescar();
        }

        void Nuevo_Clicked(object sender, EventArgs e)
        {
            if (Preferences.Get(Constants.MOSTRAR_AYUDA_GASTOS_REALES, true))
            {
                Preferences.Set(Constants.MOSTRAR_AYUDA_GASTOS_REALES, false);
                Refrescar();
            }
            Navigation.PushAsync(new AgregarGastoPage(false));
        }

        void Gasto_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            if (Preferences.Get(Constants.MOSTRAR_AYUDA_GASTOS_REALES, true))
            {
                Preferences.Set(Constants.MOSTRAR_AYUDA_GASTOS_REALES, false);
                Refrescar();
            }
            Navigation.PushAsync(new AgregarGastoPage(e.Item as Gasto));
        }

        void Anotaciones_Clicked(object sender, EventArgs e)
        {
            if (Preferences.Get(Constants.MOSTRAR_AYUDA_GASTOS_REALES, true))
            {
                Preferences.Set(Constants.MOSTRAR_AYUDA_GASTOS_REALES, false);
                Refrescar();
            }
            Navigation.PushAsync(new AnotacionesPage(DateUtil.FechaFinMes()));
        }

        void DeleteGasto_Clicked(object sender, EventArgs e)
        {
            Gasto ingreso = (Gasto)(((MenuItem)sender).CommandParameter);
            int deleted = (BindingContext as GastosRealesViewModel).DeleteGasto(ingreso);
            Refrescar();
        }

        void VerProyectados_Clicked(object sender, EventArgs e)
        {
            if (Preferences.Get(Constants.MOSTRAR_AYUDA_GASTOS_REALES, true))
            {
                Preferences.Set(Constants.MOSTRAR_AYUDA_GASTOS_REALES, false);
                Refrescar();
            }
            Navigation.PushAsync(new GastosProyectadosPage());
        }


    }
}
