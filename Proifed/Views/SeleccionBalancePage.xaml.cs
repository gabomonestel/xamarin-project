﻿using System;

using Xamarin.Forms;
using Xamarin.Essentials;
using Proifed.Models;
using Proifed.Config;
using Proifed.ViewModels;

namespace Proifed.Views
{
    public partial class SeleccionBalancePage : ContentPage
    {

        ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public SeleccionBalancePage()
        {
            InitializeComponent();
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        void General_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new BalanceGeneralPage());
        }

        void Ingresos_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new BalanceIngresosPage());
        }

        void Gastos_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new BalanceGastosPage());
        }

    }
}
