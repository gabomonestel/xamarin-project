﻿using System;
using System.Collections.Generic;
using Proifed.ViewModels;
using Xamarin.Forms;
using Proifed.Models;
using Proifed.Config;

namespace Proifed.Views
{
    public partial class BalanceGastosPage : ContentPage
    {
        ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public BalanceGastosPage()
        {
            BindingContext = new BalanceGastosViewModel(ConnectionService);
            InitializeComponent();
            CargarGastos();
        }

        private async void CargarGastos()
        {
            lstItems.IsVisible = false;
            aiLoading.IsVisible = true;
            BalanceGastosViewModel viewModel = BindingContext as BalanceGastosViewModel;
            await viewModel.CargarBalance();
            lstItems.IsVisible = true;
            aiLoading.IsVisible = false;
        }

        void FechaInicio_Unfocused(object sender, FocusEventArgs e)
        {
            var viewModel = BindingContext as BalanceGastosViewModel;
            DateTime fechaInicio = (sender as DatePicker).Date;
            viewModel.FechaInicio = new DateTime(fechaInicio.Year, fechaInicio.Month, 1);
            pickerFechaInicio.Date = viewModel.FechaInicio;

            if (viewModel.FechaFin < viewModel.FechaInicio)
            {
                viewModel.FechaFin = viewModel.FechaInicio.AddMonths(1).AddDays(-1);
                pickerFechaFin.Date = viewModel.FechaFin;
            }
            CargarGastos();
        }

        void FechaFin_Unfocused(object sender, FocusEventArgs e)
        {
            var viewModel = BindingContext as BalanceGastosViewModel;
            DateTime fechaFin = (sender as DatePicker).Date;
            viewModel.FechaFin = new DateTime(fechaFin.Year, fechaFin.Month, 1).AddMonths(1).AddDays(-1);
            pickerFechaFin.Date = viewModel.FechaFin;

            if (viewModel.FechaFin < viewModel.FechaInicio)
            {
                viewModel.FechaInicio = viewModel.FechaFin.AddMonths(-1).AddDays(1);
                pickerFechaInicio.Date = viewModel.FechaInicio;
            }
            CargarGastos();
        }

        void Consultar_Clicked(object sender, System.EventArgs e)
        {
            CargarGastos();
        }

        void Item_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            BalanceItem item = e.Item as BalanceItem;
            if (item != null)
            {
                var viewModel = BindingContext as BalanceGastosViewModel;
                switch (item.Id)
                {
                    case Constants.TIPO_PROYECTADO:
                    case Constants.TIPO_REAL:
                        Navigation.PushAsync(new ResumenGastosPage(item.Id, viewModel.FechaInicio, viewModel.FechaFin));
                        break;
                }
            }
        }

    }
}
