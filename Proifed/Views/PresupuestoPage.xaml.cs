﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Essentials;
using Proifed.Models;
using Proifed.Config;
using Newtonsoft.Json;
using Proifed.Util;

namespace Proifed.Views
{
    public partial class PresupuestoPage : TabbedPage
    {
        public PresupuestoPage()
        {
            InitializeComponent();
            this.Children.Add(new IngresosRealesPage(DateUtil.FechaFinMes()));
            this.Children.Add(new GastosRealesPage(DateUtil.FechaFinMes()));
        }

        public PresupuestoPage(int tab)
        {
            InitializeComponent();
            this.Children.Add(new IngresosRealesPage(DateUtil.FechaFinMes()));
            this.Children.Add(new GastosRealesPage(DateUtil.FechaFinMes()));
            SelectedItem = this.Children[tab];
        }

    }
}
