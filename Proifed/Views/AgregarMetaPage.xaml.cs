﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Proifed.Config;
using Proifed.ViewModels;
using Xamarin.Forms;

namespace Proifed.Views
{
    public partial class AgregarMetaPage : ContentPage
    {
        private readonly ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public AgregarMetaPage()
        {
            BindingContext = new AgregarMetaViewModel();
            ((AgregarMetaViewModel)BindingContext).InitRepo(ConnectionService);
            InitializeComponent();
            InitPickers();
        }


        void InitPickers()
        {
            pickerMes.ItemsSource = DataRepositoryUtil.obtMeses();
            pickerAnno.ItemsSource = DataRepositoryUtil.obtAnnos();
        }


        async void AgregarMeta_Clicked(object sender, System.EventArgs e)
        {
            lblRequireMes.TextColor = Color.Black;
            lblRequireAnno.TextColor = Color.Black;
            lblRequireNombre.TextColor = Color.Black;
            lblRequirePlanAccion.TextColor = Color.Black;
            lblRequireDescripcion.TextColor = Color.Black;

            string nombre = txtNombre.Text;
            string planAccion = txtPlanAccion.Text;
            string descripcion = txtDescripcion.Text;
            int mes = pickerMes.SelectedItem == null ? -1 : ((Mes)pickerMes.SelectedItem).Id;
            int anno = pickerAnno.SelectedItem == null ? -1 : (int)pickerAnno.SelectedItem;

            bool tieneErrores = false;
            if (nombre == null || nombre.Trim().Length == 0)
            {
                lblRequireNombre.TextColor = Color.Red;
                tieneErrores = true;
            }
            if (planAccion == null || planAccion.Trim().Length == 0)
            {
                lblRequirePlanAccion.TextColor = Color.Red;
                tieneErrores = true;
            }
            if (descripcion == null || descripcion.Trim().Length == 0)
            {
                lblRequireDescripcion.TextColor = Color.Red;
                tieneErrores = true;
            }
            if (mes == -1)
            {
                lblRequireMes.TextColor = Color.Red;
                tieneErrores = true;
            }
            if (anno == -1)
            {
                lblRequireAnno.TextColor = Color.Red;
                tieneErrores = true;
            }

            if (tieneErrores){
                await MostrarGuardadoCamposRequeridos();
                return;
            }

            int idMeta = (BindingContext as AgregarMetaViewModel).AddMeta(nombre, planAccion, descripcion, mes, anno);
            if(idMeta > 0){
                await Navigation.PopAsync();
            }else{
                //Error
            }
        }


        private async Task MostrarGuardadoCamposRequeridos()
        {
            stlGuardadoExitoso.IsVisible = false;
            stlCamposRequeridos.IsVisible = true;
            await Task.Delay(3000);
            stlCamposRequeridos.IsVisible = false;

        }

    }
}
