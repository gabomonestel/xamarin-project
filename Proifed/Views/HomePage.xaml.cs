﻿using System;

using Xamarin.Forms;
using Xamarin.Essentials;
using Proifed.Models;
using Proifed.Config;
using Proifed.ViewModels;

namespace Proifed.Views
{
    public partial class HomePage : ContentPage
    {

        ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public HomePage()
        {
            BindingContext = new HomeViewModel(ConnectionService);
            InitializeComponent();
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
            (BindingContext as HomeViewModel).RefreshData();
        }


        void Metas_Clicked(object sender, EventArgs e)
        {
            //((App)Application.Current).MainPage = new MetasPage();
            var metasPage = new MetasPage();
            Navigation.PushAsync(metasPage);
        }


        async void Presupuesto_Clicked(object sender, EventArgs e)
        {
            HomeViewModel viewModel = (BindingContext as HomeViewModel);
            Presupuesto presupuesto = viewModel.PresupuestoViewModel.Presupuesto;
            if (presupuesto == null)
            {
                await Navigation.PushAsync(new ParametrizacionInicialPage());
            }
            else
            {
                if (presupuesto.Configurando == true)
                {
                    bool continuar = await DisplayAlert("Configuración en proceso",
                                 "Actualmente existe una configuración sin finalizar, desea continuar configurando",
                                 "Continuar", "Nuevo");
                    if (continuar)
                    {
                        if (presupuesto.PasoActual == Constants.PASO_INGRESOS)
                        {
                            if (viewModel.TotalIngresosProyectado == 0)
                            {
                                AgregarIngresoProyectadoPage agregarIngreso = new AgregarIngresoProyectadoPage();
                                await Navigation.PushAsync(agregarIngreso);
                                Navigation.InsertPageBefore(new IngresosProyectadosPage(), agregarIngreso);
                            }
                            else
                            {
                                await Navigation.PushAsync(new IngresosProyectadosPage());
                            }
                        }
                        else if (presupuesto.PasoActual == Constants.PASO_GASTOS)
                        {
                            await Navigation.PushAsync(new GastosProyectadosPage());
                        }
                    }
                    else
                    {
                        Preferences.Set(Constants.PRESUPUESTO, "");
                        await Navigation.PushAsync(new ParametrizacionInicialPage());
                    }
                }
                else
                {
                    await Navigation.PushAsync(new PresupuestoPage());
                }
                //await Navigation.PushAsync(new PresupuestoPage());
            }
        }


        private bool checkCurrentPresupuesto(Presupuesto presupuesto)
        {
            DateTime date = DateTime.Now;

            return false;
        }

        void Ahorro_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AhorrosPage());
        }

        void Credito_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CreditosPage());
        }

        void Balance_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new SeleccionBalancePage());
        }

        async void Borrar_Clicked(object sender, EventArgs e)
        {
            bool borrar = await DisplayAlert("Borrar presupuesto", "Está seguro de borrar el presupuesto", "Sí", "No");
            if (borrar)
            {
                HomeViewModel viewModel = (BindingContext as HomeViewModel);
                int borrados = await viewModel.BorrarPresupuesto();
                await Navigation.PushAsync(new SplashPage());
                await Navigation.PopToRootAsync();
            }
        }

        async void Acerca_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AcercaPage());
        }
    }
}
