﻿using System;
using System.Collections.Generic;
using Proifed.Config;
using Proifed.Models;
using Proifed.ViewModels;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Proifed.Views
{
    public partial class AhorrosPage : ContentPage
    {
        private readonly ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public AhorrosPage()
        {
            BindingContext = new AhorrosViewModel(ConnectionService);
            InitializeComponent();
            //Preferences.Set(Constants.MOSTRAR_AYUDA_AHORROS, true);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            Refrescar();
        }


        async void Refrescar()
        {
            if (Preferences.Get(Constants.MOSTRAR_AYUDA_AHORROS, true))
            {
                stlFirstTime.IsVisible = true;
                stlEmpty.IsVisible = false;
                lstAhorros.IsVisible = false;
            }
            else
            {
                AhorrosViewModel viewModel = BindingContext as AhorrosViewModel;
                await viewModel.RefreshData();
                stlFirstTime.IsVisible = false;
                lstAhorros.IsVisible = viewModel.AhorrosData.Count > 0;
                stlEmpty.IsVisible = viewModel.AhorrosData.Count == 0;
            }
        }

        void Entendido_Clicked(object sender, EventArgs e)
        {
            Preferences.Set(Constants.MOSTRAR_AYUDA_AHORROS, false);
            Refrescar();
        }

        void AgregarAhorro_Clicked(object sender, System.EventArgs e)
        {
            if (Preferences.Get(Constants.MOSTRAR_AYUDA_AHORROS, true))
            {
                Preferences.Set(Constants.MOSTRAR_AYUDA_AHORROS, false);
                Refrescar();
            }
            Navigation.PushAsync(new AgregarAhorroPage());
        }

        void Ahorro_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            /*if (Preferences.Get(Constants.MOSTRAR_AYUDA_AHORROS, true))
            {
                Preferences.Set(Constants.MOSTRAR_AYUDA_AHORROS, false);
                Refrescar();
            }
            Navigation.PushAsync(new AgregarAhorroPage(e.Item as AhorroProyectado));*/
        }

        async void BorrarAhorro_Clicked(object sender, System.EventArgs e)
        {
            AhorroProyectado ahorro = (AhorroProyectado)(((MenuItem)sender).CommandParameter);
            var viewModel = BindingContext as AhorrosViewModel;
            int deleted = await viewModel.DeleteAhorro(ahorro);
            Refrescar();
        }
    }
}
