﻿using System;
using System.Collections.Generic;
using Proifed.ViewModels;
using Xamarin.Forms;
using Proifed.Config;
using Proifed.Models;

namespace Proifed.Views
{
    public partial class BalanceGeneralPage : ContentPage
    {
        private readonly ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public BalanceGeneralPage()
        {
            BindingContext = new BalanceGeneralViewModel(ConnectionService);
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            CargarIngresos();
        }

        private async void CargarIngresos()
        {
            lstItems.IsVisible = false;
            aiLoading.IsVisible = true;
            BalanceGeneralViewModel viewModel = BindingContext as BalanceGeneralViewModel;
            await viewModel.CargarBalance();
            lstItems.IsVisible = true;
            aiLoading.IsVisible = false;
        }

        void FechaInicio_Unfocused(object sender, FocusEventArgs e)
        {
            var viewModel = BindingContext as BalanceGeneralViewModel;
            DateTime fechaInicio = (sender as DatePicker).Date;
            viewModel.FechaInicio = new DateTime(fechaInicio.Year, fechaInicio.Month, 1);
            pickerFechaInicio.Date = viewModel.FechaInicio;

            if (viewModel.FechaFin < viewModel.FechaInicio)
            {
                viewModel.FechaFin = viewModel.FechaInicio.AddMonths(1).AddDays(-1);
                pickerFechaFin.Date = viewModel.FechaFin;
            }
            CargarIngresos();
        }

        void FechaFin_Unfocused(object sender, FocusEventArgs e)
        {
            var viewModel = BindingContext as BalanceGeneralViewModel;
            DateTime fechaFin = (sender as DatePicker).Date;
            viewModel.FechaFin = new DateTime(fechaFin.Year, fechaFin.Month, 1).AddMonths(1).AddDays(-1);
            pickerFechaFin.Date = viewModel.FechaFin;

            if (viewModel.FechaFin < viewModel.FechaInicio)
            {
                viewModel.FechaInicio = viewModel.FechaFin.AddMonths(-1).AddDays(1);
                pickerFechaInicio.Date = viewModel.FechaInicio;
            }
            CargarIngresos();
        }

        void Consultar_Clicked(object sender, System.EventArgs e)
        {
            CargarIngresos();
        }

        void Item_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            BalanceItem item = e.Item as BalanceItem;
            if (item != null)
            {
                var viewModel = BindingContext as BalanceGeneralViewModel;
                switch (item.Id)
                {
                    case Constants.TIPO_INGRESO:
                        Navigation.PushAsync(new ResumenIngresosPage(Constants.TIPO_REAL, viewModel.FechaInicio, viewModel.FechaFin));
                        break;
                    case Constants.TIPO_GASTO:
                        Navigation.PushAsync(new ResumenGastosPage(Constants.TIPO_REAL, viewModel.FechaInicio, viewModel.FechaFin));
                        break;
                    case Constants.TIPO_AHORRO:
                        Navigation.PushAsync(new ResumenAhorrosPage(viewModel.FechaInicio, viewModel.FechaFin));
                        break;
                    case Constants.TIPO_CREDITO:
                        //Navigation.PushAsync(new ResumenGastosPage(Constants.TIPO_REAL, viewModel.FechaInicio, viewModel.FechaFin));
                        break;
                }
            }
        }

        /*private async void CargarDatos()
        {

            BalanceGeneralViewModel viewModel = BindingContext as BalanceGeneralViewModel;
            double totalIngresos = await viewModel.TotalIngresosReales();
            double totalGastos = await viewModel.TotalGastosReales();
            double totalCreditos = await viewModel.TotalCreditos();
            double totalAhorros = await viewModel.TotalAhorros();

            double diferencia = totalIngresos - totalGastos - totalAhorros - totalCreditos;

            lblIngresos.Text = String.Format("{0:N}", totalIngresos);
            lblGastos.Text = String.Format("{0:N}", totalGastos);
            lblCreditos.Text = String.Format("{0:N}", totalCreditos);
            lblAhorros.Text = String.Format("{0:N}", totalAhorros);

            lblDiferencia.Text = String.Format("{0:N}", diferencia >= 0 ? diferencia : -diferencia);

            if (diferencia >= 0)
            {
                lblDiferencia.TextColor = Color.Green;
            }
            else
            {
                lblDiferencia.TextColor = Color.Red;
            }

        }*/
    }
}
