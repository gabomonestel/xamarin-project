﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Proifed.Config;
using Proifed.Models;
using Proifed.Util;
using Proifed.ViewModels;
using Xamarin.Forms;

namespace Proifed.Views
{
    public partial class AgregarGastoProyectadoPage : ContentPage
    {
        private readonly ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public AgregarGastoProyectadoPage()
        {
            var agregarGastoVM = new AgregarGastoProyectadoViewModel(ConnectionService);
            BindingContext = agregarGastoVM;
            InitializeComponent();
            InitPickers();
        }

        public AgregarGastoProyectadoPage(GastoProyectado gasto)
        {
            var agregarGastoVM = new AgregarGastoProyectadoViewModel(ConnectionService, gasto);
            BindingContext = agregarGastoVM;
            InitializeComponent();
            InitPickers();
            CargarGasto();
        }

        private void CargarGasto()
        {
            AgregarGastoProyectadoViewModel viewModel = (BindingContext as AgregarGastoProyectadoViewModel);
            foreach (RubroGasto rubro in pickerRubro.ItemsSource)
            {
                if (rubro.Id == viewModel.Gasto.Rubro)
                {
                    pickerRubro.SelectedItem = rubro;
                    pickerTipo.ItemsSource = rubro.Gastos;
                    foreach (DetalleGasto detalle in rubro.Gastos)
                    {
                        if (detalle.Id == viewModel.Gasto.Tipo)
                        {
                            pickerTipo.SelectedItem = detalle;
                            break;
                        }
                    }
                    break;
                }
            }
            DateTime fechaActual = DateUtil.FechaFinMes();
            DateTime fechaFinalizacion = viewModel.Gasto.FechaFinalizacion;
            int diferenciaMeses = DataRepositoryUtil.DiferenciaMeses(fechaActual, fechaFinalizacion);
            switch (diferenciaMeses)
            {
                case 0:
                    pickerPlazo.SelectedIndex = 0; break;
                case 2:
                    pickerPlazo.SelectedIndex = 1; break;
                case 5:
                    pickerPlazo.SelectedIndex = 2; break;
                case 11:
                    pickerPlazo.SelectedIndex = 3; break;
                default:
                    pickerPlazo.SelectedIndex = 0; break;
            }
            txtNombre.Text = viewModel.Gasto.Nombre;
            txtMonto.Text = viewModel.Gasto.Monto.ToString();

        }

        private void InitPickers()
        {
            var rubros = DataRepositoryUtil.obtRubrosGasto();
            pickerRubro.ItemsSource = rubros;
            pickerTipo.ItemsSource = rubros[0].Gastos;
            pickerPlazo.ItemsSource = DataRepositoryUtil.obtPlazos();
            pickerRubro.SelectedIndex = 0;
            pickerTipo.SelectedIndex = 0;
            pickerPlazo.SelectedIndex = 0;
        }


        private void ResetFormCreate()
        {
            txtNombre.IsVisible = false;
            lblNombre.IsVisible = false;
            txtMonto.Text = "";
            txtNombre.Text = "";
            pickerRubro.SelectedIndex = 0;
            pickerTipo.ItemsSource = (pickerRubro.SelectedItem as RubroGasto).Gastos;
            pickerTipo.SelectedIndex = 0;
            pickerPlazo.SelectedIndex = 0;
        }

        void Rubro_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            RubroGasto rubro = ((Picker)sender).SelectedItem as RubroGasto;
            pickerTipo.ItemsSource = rubro.Gastos;
            pickerTipo.SelectedIndex = 0;
        }

        void Tipo_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            DetalleGasto detalle = ((Picker)sender).SelectedItem as DetalleGasto;
            if (detalle != null)
            {
                if (detalle.Nombre.Equals("Otros"))
                {
                    txtNombre.Text = "";
                    lblNombre.IsVisible = true;
                    txtNombre.IsVisible = true;
                    lblRequireNombre.IsVisible = true;
                }
                else
                {
                    lblNombre.IsVisible = false;
                    txtNombre.IsVisible = false;
                    txtNombre.Text = detalle.Nombre;
                    lblRequireNombre.IsVisible = false;
                }
            }
        }

        private async void Guardar_Clicked(object sender, EventArgs e)
        {
            int plazo = pickerPlazo.SelectedItem != null ? (pickerPlazo.SelectedItem as Plazo).Cantidad : -1;
            int rubro = (pickerRubro.SelectedItem as RubroGasto) != null ? (pickerRubro.SelectedItem as RubroGasto).Id : -1;
            int tipo = (pickerTipo.SelectedItem as DetalleGasto) != null ? (pickerTipo.SelectedItem as DetalleGasto).Id : -1;
            double monto = !string.IsNullOrEmpty(txtMonto.Text) ? double.Parse(txtMonto.Text) : -1;
            string nombre = !string.IsNullOrEmpty(txtNombre.Text) ? txtNombre.Text : "";


            bool tieneErrores = false;

            if (plazo == -1)
            {
                lblRequirePlazo.TextColor = Color.Red;
                tieneErrores = true;
            }
            if (rubro == -1)
            {
                lblRequireRubro.TextColor = Color.Red;
                tieneErrores = true;
            }
            if (tipo == -1)
            {
                lblRequireTipo.TextColor = Color.Red;
                tieneErrores = true;
            }
            if (monto < 0)
            {
                lblRequireMonto.TextColor = Color.Red;
                tieneErrores = true;
            }
            if (nombre.Equals(""))
            {
                lblRequireNombre.TextColor = Color.Red;
                tieneErrores = true;
            }

            if (tieneErrores)
            {
                await MostrarGuardadoCamposRequeridos();
                return;
            }

            var viewModel = BindingContext as AgregarGastoProyectadoViewModel;

            GastoProyectado gasto;
            if (viewModel.Gasto != null)
            {
                gasto = viewModel.Gasto;
            }
            else
            {
                gasto = new GastoProyectado();
            }

            gasto.Rubro = rubro;
            gasto.Tipo = tipo;
            gasto.Monto = monto;
            gasto.Nombre = nombre;
            if (plazo != -1)
            {
                DateTime finaliza = DateUtil.FechaFinMes().AddMonths(plazo - 1);
                gasto.FechaCreacion = DateUtil.FechaFinMes();
                gasto.FechaFinalizacion = finaliza;
            }

            int idGasto = await viewModel.GuardarGastoProyectado(gasto, plazo);
            if (idGasto > 0)
            {
                MostrarGuardadoExitoso();
            }
            else
            {
                //Error
            }
        }

        private void MostrarGuardadoExitoso()
        {
            /*resetFormCreate();
            stlCamposRequeridos.IsVisible = false;
            stlGuardadoExitoso.IsVisible = true;
            await Task.Delay(3000);
            stlGuardadoExitoso.IsVisible = false;*/
            Navigation.PopAsync();
        }

        private async Task MostrarGuardadoCamposRequeridos()
        {
            stlGuardadoExitoso.IsVisible = false;
            stlCamposRequeridos.IsVisible = true;
            await Task.Delay(3000);
            stlCamposRequeridos.IsVisible = false;

        }

    }
}
