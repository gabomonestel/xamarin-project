﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Proifed.Views;
using Proifed.ViewModels;
using Proifed.Models;
using Proifed.Config;
using Xamarin.Essentials;

namespace Proifed.Views
{
    public partial class CreditosPage : ContentPage
    {
        private readonly ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public CreditosPage()
        {
            BindingContext = new CreditosViewModel(ConnectionService);
            InitializeComponent();
            //Preferences.Set(Constants.MOSTRAR_AYUDA_CREDITOS, true);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            Refrescar();
        }


        async void Refrescar(){
            if (Preferences.Get(Constants.MOSTRAR_AYUDA_CREDITOS, true))
            {
                stlFirstTime.IsVisible = true;
                stlEmpty.IsVisible = false;
                lstCreditos.IsVisible = false;
            }
            else
            {
                CreditosViewModel viewModel = BindingContext as CreditosViewModel;
                await viewModel.RefreshData();
                stlFirstTime.IsVisible = false;
                lstCreditos.IsVisible = viewModel.CreditosData.Count > 0;
                stlEmpty.IsVisible = viewModel.CreditosData.Count == 0;
            }
        }

        void AgregarCredito_Clicked(object sender, System.EventArgs e)
        {
            if (Preferences.Get(Constants.MOSTRAR_AYUDA_CREDITOS, true))
            {
                Preferences.Set(Constants.MOSTRAR_AYUDA_CREDITOS, false);
                Refrescar();
            }
            Navigation.PushAsync(new AgregarCreditoPage());
        }

        void Entendido_Clicked(object sender, EventArgs e)
        {
            Preferences.Set(Constants.MOSTRAR_AYUDA_CREDITOS, false);
            Refrescar();
        }

        async void BorrarCredito_Clicked(object sender, System.EventArgs e)
        {
            Credito credito = (Credito)(((MenuItem)sender).CommandParameter);
            var viewModel = BindingContext as CreditosViewModel;
            int deleted = await viewModel.DeleteCredito(credito);
            Refrescar();
        }
    }
}
