﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Proifed.Config;
using Proifed.Models;
using Proifed.Util;
using Proifed.ViewModels;
using Xamarin.Forms;

namespace Proifed.Views
{
    public partial class AgregarIngresoPage : ContentPage
    {
        private readonly ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public AgregarIngresoPage()
        {
            BindingContext = new AgregarIngresoViewModel(ConnectionService);
            InitializeComponent();
            InitPickers();
        }

        public AgregarIngresoPage(Ingreso ingreso)
        {
            BindingContext = new AgregarIngresoViewModel(ConnectionService, ingreso);
            InitializeComponent();
            InitPickers();
            CargarIngreso();
        }

        private void CargarIngreso(){
            AgregarIngresoViewModel viewModel = (BindingContext as AgregarIngresoViewModel);
            foreach (RubroIngreso rubro in pickerTipo.ItemsSource){
                if (rubro.Id == viewModel.Ingreso.Tipo){
                    pickerTipo.SelectedItem = rubro;
                    break;
                }
            }
            txtNombre.Text = viewModel.Ingreso.Nombre;
            txtMonto.Text = viewModel.Ingreso.Monto.ToString();

        }



        void InitPickers()
        {
            pickerTipo.ItemsSource = DataRepositoryUtil.obtTiposIngreso();
            ResetFormCreate();

        }

        void Cancel_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PopAsync();
        }

        private async void Guardar_Clicked(object sender, System.EventArgs e)
        {
            AgregarIngresoViewModel viewModel = BindingContext as AgregarIngresoViewModel;

            RubroIngreso rubro = pickerTipo.SelectedItem as RubroIngreso;
            int tipo = rubro != null ? rubro.Id : -1;
            double monto = txtMonto.Text != null && txtMonto.Text != "" ? double.Parse(txtMonto.Text) : -1;
            string nombre = txtNombre.Text != null && txtNombre.Text != "" ? txtNombre.Text : "";


            bool tieneErrores = false;

            if (tipo == -1)
            {
                lblRequireTipo.TextColor = Color.Red;
                tieneErrores = true;
            }
            if (monto < 0)
            {
                lblRequireMonto.TextColor = Color.Red;
                tieneErrores = true;
            }
            if (nombre.Equals(""))
            {
                lblRequireNombre.TextColor = Color.Red;
                tieneErrores = true;
            }

            if (tieneErrores)
            {
                await MostrarGuardadoCamposRequeridos();
                return;
            }

            Ingreso ingreso;
            if(viewModel.Ingreso != null){
                ingreso = viewModel.Ingreso;
                ingreso.ModificadoUsuario = true;
            }else{
                ingreso = new Ingreso();
            }

            ingreso.Tipo = tipo;
            ingreso.EsReal = true;
            ingreso.Monto = monto;
            ingreso.Nombre = nombre;
            ingreso.FechaCreacion = DateUtil.FechaFinMes();

            int idIngreso = await viewModel.GuardarIngreso(ingreso);
            if (idIngreso > 0)
            {
                MostrarGuardadoExitoso();
            }
            else
            {
                //Error
            }
        }



        void Tipo_SelectedIndexChanged(object sender, System.EventArgs e)
        {

            RubroIngreso rubro = ((Picker)sender).SelectedItem as RubroIngreso;
            if (rubro != null)
            {
                if (rubro.Nombre.Equals("Otros"))
                {
                    txtNombre.Text = "";
                    lblNombre.IsVisible = true;
                    txtNombre.IsVisible = true;
                    lblRequireNombre.IsVisible = true;
                }
                else
                {
                    lblNombre.IsVisible = false;
                    txtNombre.IsVisible = false;
                    txtNombre.Text = rubro.Nombre;
                    lblRequireNombre.IsVisible = false;
                }
            }
        }

        void ResetFormCreate()
        {
            txtNombre.IsVisible = false;
            lblNombre.IsVisible = false;
            txtMonto.Text = "";
            txtNombre.Text = "";
            pickerTipo.SelectedIndex = -1;
        }

        private void MostrarGuardadoExitoso()
        {
            /*resetFormCreate();
            stlCamposRequeridos.IsVisible = false;
            stlGuardadoExitoso.IsVisible = true;
            await Task.Delay(3000);
            stlGuardadoExitoso.IsVisible = false;*/
            Navigation.PopAsync();
        }

        private async Task MostrarGuardadoCamposRequeridos()
        {
            stlGuardadoExitoso.IsVisible = false;
            stlCamposRequeridos.IsVisible = true;
            await Task.Delay(3000);
            stlCamposRequeridos.IsVisible = false;

        }
    }
}
