﻿using System;
using System.Collections.Generic;
using Proifed.Models;
using Proifed.ViewModels;
using Xamarin.Forms;
using Proifed.Config;

namespace Proifed.Views
{
    public partial class BalanceIngresosPage : ContentPage
    {
        private readonly ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public BalanceIngresosPage()
        {
            BindingContext = new BalanceIngresosViewModel(ConnectionService);
            InitializeComponent();
            CargarIngresos();
        }

        private async void CargarIngresos()
        {
            lstItems.IsVisible = false;
            aiLoading.IsVisible = true;
            BalanceIngresosViewModel viewModel = BindingContext as BalanceIngresosViewModel;
            await viewModel.CargarBalance();
            lstItems.IsVisible = true;
            aiLoading.IsVisible = false;
        }

        void FechaInicio_Unfocused(object sender, FocusEventArgs e)
        {
            var viewModel = BindingContext as BalanceIngresosViewModel;
            DateTime fechaInicio = (sender as DatePicker).Date;
            viewModel.FechaInicio = new DateTime(fechaInicio.Year, fechaInicio.Month, 1);
            pickerFechaInicio.Date = viewModel.FechaInicio;

            if (viewModel.FechaFin < viewModel.FechaInicio)
            {
                viewModel.FechaFin = viewModel.FechaInicio.AddMonths(1).AddDays(-1);
                pickerFechaFin.Date = viewModel.FechaFin;
            }
            CargarIngresos();
        }

        void FechaFin_Unfocused(object sender, FocusEventArgs e)
        {
            var viewModel = BindingContext as BalanceIngresosViewModel;
            DateTime fechaFin = (sender as DatePicker).Date;
            viewModel.FechaFin = new DateTime(fechaFin.Year, fechaFin.Month, 1).AddMonths(1).AddDays(-1);
            pickerFechaFin.Date = viewModel.FechaFin;

            if (viewModel.FechaFin < viewModel.FechaInicio)
            {
                viewModel.FechaInicio = viewModel.FechaFin.AddMonths(-1).AddDays(1);
                pickerFechaInicio.Date = viewModel.FechaInicio;
            }
            CargarIngresos();
        }

        void Consultar_Clicked(object sender, System.EventArgs e)
        {
            CargarIngresos();
        }

        void Item_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            BalanceItem item = e.Item as BalanceItem;
            if(item != null)
            {
                var viewModel = BindingContext as BalanceIngresosViewModel;
                switch (item.Id) {
                    case Constants.TIPO_PROYECTADO:
                    case Constants.TIPO_REAL:
                        Navigation.PushAsync(new ResumenIngresosPage(item.Id, viewModel.FechaInicio, viewModel.FechaFin));
                        break;
                }
            }
        }

    }
}
