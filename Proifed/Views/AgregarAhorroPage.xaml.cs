﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Proifed.Config;
using Proifed.Models;
using Proifed.Util;
using Proifed.ViewModels;
using Xamarin.Forms;

namespace Proifed.Views
{
    public partial class AgregarAhorroPage : ContentPage
    {
        private readonly ISQLiteConnection ConnectionService = DependencyService.Get<ISQLiteConnection>();

        public AgregarAhorroPage()
        {
            BindingContext = new AgregarAhorroViewModel(ConnectionService);
            InitializeComponent();
            InitPickers();
        }

        public AgregarAhorroPage(AhorroProyectado ahorro)
        {
            BindingContext = new AgregarAhorroViewModel(ConnectionService, ahorro); ;
            InitializeComponent();
            InitPickers();
        }

        private void InitPickers()
        {
            pickerPlazo.ItemsSource = DataRepositoryUtil.obtPlazosAhorro();
        }

        private void MostrarGuardar(bool mostrar)
        {
            txtAporteInicial.IsEnabled = !mostrar;
            txtMontoMensual.IsEnabled = !mostrar;
            pickerPlazo.IsEnabled = !mostrar;
            lblInteresAnual.IsVisible = mostrar;
            txtInteresAnual.IsVisible = mostrar;
            lblAhorroTotal.IsVisible = mostrar;
            txtAhorroTotal.IsVisible = mostrar;
            btnCalcular.IsVisible = !mostrar;
            btnGuardar.IsVisible = mostrar;
            btnEditar.IsVisible = mostrar;
            btnLimpiar.IsVisible = mostrar;
        }

        void Editar_Clicked(object sender, System.EventArgs e)
        {
            MostrarGuardar(false);
        }

        void Limpiar_Clicked(object sender, System.EventArgs e)
        {
            pickerPlazo.SelectedIndex = -1;
            txtAporteInicial.Text = "";
            txtMontoMensual.Text = "";
            MostrarGuardar(false);
        }

        async void CalcularAhorroTotal_Clicked(object sender, System.EventArgs e)
        {
            lblRequireNombre.TextColor = Color.Black;
            lblRequireAporte.TextColor = Color.Black;
            lblRequireMonto.TextColor = Color.Black;
            lblRequirePlazo.TextColor = Color.Black;

            string nombre = string.IsNullOrEmpty(txtNombre.Text) ? null : txtNombre.Text.Trim();
            double aporteInicial = String.IsNullOrEmpty(txtAporteInicial.Text) ? -1 : Double.Parse(txtAporteInicial.Text);
            double montoMensual = String.IsNullOrEmpty(txtMontoMensual.Text) ? 0 : Double.Parse(txtMontoMensual.Text);
            PlazoAhorro plazo = pickerPlazo.SelectedItem as PlazoAhorro;

            bool tieneErrores = false;


            if (nombre == null)
            {
                lblRequireNombre.TextColor = Color.Red;
                tieneErrores = true;
            }
            if (aporteInicial < 0)
            {
                lblRequireAporte.TextColor = Color.Red;
                tieneErrores = true;
            }
            if (montoMensual <= 0)
            {
                lblRequireMonto.TextColor = Color.Red;
                tieneErrores = true;
            }
  
            if (plazo == null)
            {
                lblRequirePlazo.TextColor = Color.Red;
                tieneErrores = true;
            }

            if (tieneErrores)
            {
                await MostrarCalcularCamposRequeridos();
                return;
            }

            double montoTotal = montoMensual * ((Math.Pow(1 + plazo.InteresMensual, plazo.CantidadMeses) - 1) / plazo.InteresMensual) 
                                + aporteInicial * Math.Pow(1+plazo.InteresMensual, plazo.CantidadMeses);
            txtInteresAnual.Text = String.Format("{0:N}", plazo.InteresAnual);
            txtAhorroTotal.Text = String.Format("{0:N}", montoTotal);

            MostrarGuardar(true);
            //Mt = Mm * 1 + it - 1i + M01 + it

        }

        async void Guardar_Clicked(object sender, System.EventArgs e)
        {
            lblRequireNombre.TextColor = Color.Black;
            lblRequireAporte.TextColor = Color.Black;
            lblRequireMonto.TextColor = Color.Black;
            lblRequirePlazo.TextColor = Color.Black;

            string nombre = string.IsNullOrEmpty(txtNombre.Text) ? null : txtNombre.Text.Trim();
            double aporteInicial = string.IsNullOrEmpty(txtAporteInicial.Text) ? -1 : Double.Parse(txtAporteInicial.Text);
            double montoMensual = string.IsNullOrEmpty(txtMontoMensual.Text) ? 0 : Double.Parse(txtMontoMensual.Text);
            PlazoAhorro plazo = pickerPlazo.SelectedItem as PlazoAhorro;

            bool tieneErrores = false;

            if (nombre == null)
            {
                lblRequireNombre.TextColor = Color.Red;
                tieneErrores = true;
            }
            if (aporteInicial < 0)
            {
                lblRequireAporte.TextColor = Color.Red;
                tieneErrores = true;
            }
            if (montoMensual <= 0)
            {
                lblRequireMonto.TextColor = Color.Red;
                tieneErrores = true;
            }
            if (plazo == null)
            {
                lblRequirePlazo.TextColor = Color.Red;
                tieneErrores = true;
            }

            if(tieneErrores)
            {
                await MostrarCalcularCamposRequeridos();
                return;
            }

            var viewModel = BindingContext as AgregarAhorroViewModel;

            AhorroProyectado ahorro;
            if (viewModel.Ahorro != null)
            {
                ahorro = viewModel.Ahorro;
            }
            else
            {
                ahorro = new AhorroProyectado();
            }

            ahorro.AporteInicial = aporteInicial;
            ahorro.Monto = montoMensual;
            ahorro.InteresAnnual = plazo.InteresAnual;
            ahorro.InteresMensual = plazo.InteresMensual;
            ahorro.Nombre = nombre;
            ahorro.Descripcion = "Mensualidad";

            DateTime finaliza = DateUtil.FechaFinMes().AddMonths(plazo.CantidadMeses);
            ahorro.FechaCreacion = DateUtil.FechaFinMes();
            ahorro.FechaFinalizacion = finaliza;


            int idAhorro = await viewModel.GuardarAhorroProyectado(ahorro, plazo.CantidadMeses);
            if (idAhorro > 0)
            {
                await Navigation.PopAsync();
            }
            else
            {
                await DisplayAlert("Error", "Algo salió mal", "Cerrar");
            }

        }

        private async Task MostrarCalcularCamposRequeridos()
        {
            stlCamposRequeridos.IsVisible = true;
            await Task.Delay(3000);
            stlCamposRequeridos.IsVisible = false;

        }

    }
}
